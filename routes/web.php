<?php

Route::get('/', function () {
    return view('login.login');
});

Auth::routes();
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/dashboard/total_clients', 'DashboardController@total_clients')->name('dashboard.total_clients');
Route::get('/dashboard/total_bill', 'DashboardController@total_bill')->name('dashboard.total_bill');
Route::get('/dashboard/monthly_bill', 'DashboardController@monthly_bill')->name('dashboard.monthly_bill');
//========================== Client Routes ==============================//
Route::get('/client', 'ClientController@index')->name('client.index');
Route::get('/client/create', 'ClientController@create')->name('client.create');
Route::get('/client/form_data', 'ClientController@form_data')->name('client.form_data');
Route::post('/client/store', 'ClientController@store')->name('client.store');
Route::get('/client/payment_receive', 'ClientController@payment_receive')->name('client.payment_receive');
Route::get('/client/show', 'ClientController@show')->name('client.show');
Route::get('/client/view', 'ClientController@view')->name('client.view');
Route::get('/client/edit', 'ClientController@edit')->name('client.edit');
Route::post('/client/update', 'ClientController@update')->name('client.update');
Route::get('/client/delete_view', 'ClientController@delete_view')->name('client.delete_view');
Route::delete('/client/destroy', 'ClientController@delete')->name('client.destroy');
//========================== Client Routes ==============================//
Route::get('/client_report', 'ClientReportController@index')->name('client_report.index');
Route::get('/client_report/create', 'ClientReportController@create')->name('client_report.create');
//========================== Bill & Payment Routes ==============================//
Route::get('/bill_payment/', 'BillPaymentController@index')->name('bill_payment.index');
Route::post('/bill_payment/store', 'BillPaymentController@store')->name('bill_payment.store');
Route::get('/bill_payment/show', 'BillPaymentController@show')->name('bill_payment.show');
Route::get('/bill_payment/view', 'BillPaymentController@view')->name('bill_payment.view');
Route::get('/bill_payment/edit', 'BillPaymentController@edit')->name('bill_payment.edit');
Route::post('/bill_payment/update', 'BillPaymentController@update')->name('bill_payment.update');
Route::get('/bill_payment/delete_view', 'BillPaymentController@delete_view')->name('bill_payment.delete_view');
Route::delete('/bill_payment/destroy', 'BillPaymentController@delete')->name('bill_payment.destroy');
//========================== Employee Routes ==============================//
Route::get('/employee', 'EmployeeController@index')->name('employee.index');
Route::get('/employee/form_data', 'EmployeeController@form_data')->name('employee.form_data');
Route::post('/employee/store', 'EmployeeController@store')->name('employee.store');
Route::get('/employee/show', 'EmployeeController@show')->name('employee.show');
Route::get('/employee/view', 'EmployeeController@view')->name('employee.view');
Route::get('/employee/edit', 'EmployeeController@edit')->name('employee.edit');
Route::post('/employee/update', 'EmployeeController@update')->name('employee.update');
Route::get('/employee/delete_view', 'EmployeeController@delete_view')->name('employee.delete_view');
Route::delete('/employee/destroy', 'EmployeeController@delete')->name('employee.destroy');
//========================== Package Routes ==============================//
Route::get('/package', 'PackageController@index')->name('package.index');
Route::post('/package/store', 'PackageController@store')->name('package.store');
Route::get('/package/show', 'PackageController@show')->name('package.show');
Route::get('/package/edit', 'PackageController@edit')->name('package.edit');
Route::put('/package/update', 'PackageController@update')->name('package.update');
Route::get('/package/delete_view', 'PackageController@delete_view')->name('package.delete_view');
Route::delete('/package/destroy', 'PackageController@delete')->name('package.destroy');
//========================== Area Routes ==============================//
Route::get('/area', 'AreaController@index')->name('area.index');
Route::post('/area/store', 'AreaController@store')->name('area.store');
Route::get('/area/show', 'AreaController@show')->name('area.show');
Route::get('/area/edit', 'AreaController@edit')->name('area.edit');
Route::put('/area/update', 'AreaController@update')->name('area.update');
Route::get('/area/delete_view', 'AreaController@delete_view')->name('area.delete_view');
Route::delete('/area/destroy', 'AreaController@delete')->name('area.destroy');
//========================== Expense Routes ==============================//
Route::get('/expense', 'ExpenseController@index')->name('expense.index');
Route::post('/expense/store', 'ExpenseController@store')->name('expense.store');
Route::get('/expense/show', 'ExpenseController@show')->name('expense.show');
Route::get('/expense/edit', 'ExpenseController@edit')->name('expense.edit');
Route::put('/expense/update', 'ExpenseController@update')->name('expense.update');
Route::get('/expense/delete_view', 'ExpenseController@delete_view')->name('expense.delete_view');
Route::delete('/expense/destroy', 'ExpenseController@delete')->name('expense.destroy');

