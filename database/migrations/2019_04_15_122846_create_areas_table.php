<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAreasTable extends Migration
{

    public function up()
    {
        Schema::create( 'areas' , function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->string( 'name', 100);
            $table->tinyInteger( 'status' )->default(1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('areas');
    }
}
