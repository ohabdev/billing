<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillPaymentsTable extends Migration
{

    public function up()
    {
        Schema::create('bill_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('billId', 50);
            $table->unsignedBigInteger('clients_id');
            $table->integer('year');
            $table->integer('month');
            $table->decimal('total_amt', 10,2);
            $table->decimal('current_amt', 10,2);
            $table->decimal('paid_amt', 10,2);
            $table->decimal('due_amt', 10,2);
            $table->date('payment_deadline');
            $table->foreign('clients_id')->references('id')->on('clients');
            $table->timestamps();
        });
    }
        
    public function down()
    {
        Schema::dropIfExists('bill_payments');
    }
}
