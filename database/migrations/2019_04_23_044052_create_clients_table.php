<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    public function up()
    {
        Schema::create( 'clients' , function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->string( 'client_id', 100);
            $table->string( 'nid', 100)->nullable();
            $table->string( 'name', 100);
            $table->string( 'image', 150)->default("default_client_img.png");
            $table->string( 'area_code', 50)->nullable();
            $table->string( 'billing_address', 150);
            $table->string( 'connection_address', 150);
            $table->string( 'contact_number', 24);
            $table->date( 'connection_date');
            $table->decimal( 'connection_fee', 8,2);
            $table->tinyInteger( 'status' )->default(1);
            $table->unsignedBigInteger( 'packages_id' );
            $table->unsignedBigInteger( 'areas_id');
            $table->decimal( 'monthly_bill', 8,2 );
            $table->foreign( 'packages_id' )->references( 'id' )->on( 'packages' );
            $table->foreign( 'areas_id' )->references( 'id' )->on( 'areas' );
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('clients');
    }
}