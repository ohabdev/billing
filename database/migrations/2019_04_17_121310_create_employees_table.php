<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    public function up()
    {
        Schema::create( 'employees' , function( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->string( 'name', 100);
            $table->date( 'hire_date');
            $table->string( 'phone_number', 25);
            $table->string( 'password', 150);
            $table->string( 'image', 150);
            $table->string( 'address', 150);
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
