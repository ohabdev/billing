<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
        	'name'=> 'Ohab Riaz',
        	'email'=> 'ohab@gmail.com',
        	'usertype'=> 'admin',
        	'password'=> bcrypt('123456'),
        ]);
    }
}
