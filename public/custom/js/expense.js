$(document).ready(function(){

  	//=========== Add Expense Script======================
  	
  	$("#expense_add_data").on("submit", function(event){
		event.preventDefault();
		var url = $(this).attr('action');
		var type = $(this).attr('method');
		var formData = new FormData($(this)[0]);
		$.ajax({
			url: url,
	       	type: type,
	       	data: formData,
	       	async: false,
	       	cache: false,
	      	contentType: false,
	       	enctype: 'multipart/form-data',
	       	processData: false,
			success: function(data){
				$.each(data.errors, function(key, value){
                  $('#expense_add_errors').append('<li><span style="color:red;"><b>Error!</b> '+value+'</span></li>');
                 });
				$("#expense_add_errors").hide();
				if(data == 'success')
				{
					swal("Congrats: Saved!", "New Expense Added Successfully!", "success");
				}
				$("#expense_add_data").trigger('reset');
				return LoadAllExpense();
			}
		});
	});

  	//==================== Load All Expense Script======================//
  	
	LoadAllExpense();

	function LoadAllExpense(){
		$.ajax({
			url: 'expense/show',
			method: "GET",
			dataType:"JSON",
			success: function(data){
				
				var row_data = '';
				var sl = 1;
				var i = 0;
				for(i=0; i<data.length; i++)
				{
					row_data += '<tr><td>'+ sl++ +'</td>';
					row_data += '<td>'+data[i].name+'</td>';
					if (data[i].status == 1)
					{
						row_data += '<td><i class="fa fa-check-circle" style="color:green"></i></td>';
					}else{
						row_data += '<td><i class="fa fa-close" style="color:red"></i></td>';
					}
					row_data += '<td>';
					row_data += '<a href="expense/edit" id="expense_edit" data-id="'+data[i].id+'" data-toggle="modal" data-target="#expense_edit_modal" style="color:#17a2b8; margin:5px;"><i class="fa fa-edit"></i></a>';
					row_data += '<a href="expense/delete_view" id="expense_delete" data-id="'+data[i].id+'" data-toggle="modal" data-target="#expense_delete_modal" style="color:red; margin:5px;"><i class="fa fa-trash"></i></a>';
					row_data += '</td>';
					row_data += '</tr>';
				}
				$("#show_all_expense").html(row_data);
			}	
		});
	}

	//==================== Expense Edit Script ======================//
	
	$(document).on('click', '#expense_edit', function(e){
	 	e.preventDefault();
	 	var id = $(this).data('id');
	 	var url = $(this).attr('href');
	 	$.ajax({
	 		url: url,
	 		data: {id:id},
	 		dataTyep: "JSON",
	 		success: function(data){
	 			$("#expense_edit_modal").modal("show");
	 			$("#expense_edit_id").val(data.id);
	 			$("#expense_edit_name").val(data.name);
	 			$("#expense_edit_status").val(data.status);
	 		}
	 	});
	});

	//==================== Expense Update Script ======================//

	$("#expense_update_data").on("submit", function(event){
		event.preventDefault();
		var url = $(this).attr('action');
		var type = $(this).attr('method');
		var data = $(this).serialize();
		$.ajax({
			url : url,
			method: type,
			data: data,
			dataType: "JSON",
			success: function(data){
				$.each(data.errors, function(key, value){
                  $('#expense_update_errors').append('<li><span style="color:red;"><b>Error!</b> '+value+'</span></li>');
                 });
				if(data == 'success'){
					$("#expense_edit_modal").modal("hide");
					swal("Congrats: Updated!", "Expense Update Successfully!", "success");
					return LoadAllExpense();
					$("#expense_udpate_data").trigger('reset');
				}
			}
		});
	});

	//==================== Expense delete Script ======================//
	
	$(document).on('click', '#expense_delete', function(e){
	 	e.preventDefault();
	 	var id = $(this).data('id');
	 	var url = $(this).attr('href');
	 	$.ajax({
	 		url: url,
	 		data: {id:id},
	 		dataTyep: "JSON",
	 		success: function(data){
	 			$("#expense_delete_modal").modal("show");
	 			$("#expense_delete_id").val(data.id);
	 		}
	 	});
	});

	//==================== Expense Update Script ======================//

	$("#expense_delete_data").on("submit", function(event){
		event.preventDefault();
		var url = $(this).attr('action');
		var type = $(this).attr('method');
		var data = $(this).serialize();
		$.ajax({
			url : url,
			method: type,
			data: data,
			dataType: "JSON",
			success: function(data){
				if(data == 'success'){
					$("#expense_delete_modal").modal("hide");
					swal("Congrats: Deleted!", "Expense Deleted Successfully!", "success");
					return LoadAllExpense();
					$("#expense_delete_data").trigger('reset');
				}
			}
		});
	});



});