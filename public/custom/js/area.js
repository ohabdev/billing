$(document).ready(function(){

  	//=========== Add Area Script======================
  	
  	$("#area_add_data").on("submit", function(event){
		event.preventDefault();
		var url = $(this).attr('action');
		var type = $(this).attr('method');
		var formData = new FormData($(this)[0]);
		$.ajax({
			url: url,
	       	type: type,
	       	data: formData,
	       	async: false,
	       	cache: false,
	      	contentType: false,
	       	enctype: 'multipart/form-data',
	       	processData: false,
			success: function(data){
				$.each(data.errors, function(key, value){
                  $('#area_add_errors').append('<li><span style="color:red;"><b>Error!</b> '+value+'</span></li>');
                 });
				if(data == 'success')
				{
					swal("Congrats: Saved!", "New Area Added Successfully!", "success");
				}
				$("#area_add_data").trigger('reset');
				return LoadAllArea();
			}
		});
	});

  	//==================== Load All Area Script======================//
  	
	LoadAllArea();

	function LoadAllArea(){
		$.ajax({
			url: 'area/show',
			method: "GET",
			success: function(data){
				var row_data = '';
				var sl = 1;
				var i = 0;
				for(i=0; i<data.length; i++)
				{
					row_data += '<tr><td>'+ sl++ +'</td>';
					row_data += '<td>'+data[i].name+'</td>';
					if (data[i].status == 1)
					{
						row_data += '<td><i class="fa fa-check-circle" style="color:green"></i></td>';
					}else{
						row_data += '<td><i class="fa fa-close" style="color:red"></i></td>';
					}
					row_data += '<td>';
					row_data += '<a href="area/edit" id="area_edit" data-id="'+data[i].id+'" data-toggle="modal" data-target="#area_edit_modal" style="color:#17a2b8; margin:5px;"><i class="fa fa-edit"></i></a>';
					row_data += '<a href="area/delete_view" id="area_delete" data-id="'+data[i].id+'" data-toggle="modal" data-target="#area_delete_modal" style="color:red; margin:5px;"><i class="fa fa-trash"></i></a>';
					row_data += '</td>';
					row_data += '</tr>';
				}
				$("#show_all_area").html(row_data);
			}	
		});
	}

	//==================== Package Edit Script ======================//
	
	$(document).on('click', '#area_edit', function(e){
	 	e.preventDefault();
	 	var id = $(this).data('id');
	 	var url = $(this).attr('href');
	 	$.ajax({
	 		url: url,
	 		data: {id:id},
	 		dataTyep: "JSON",
	 		success: function(data){
	 			$("#area_edit_modal").modal("show");
	 			$("#area_edit_id").val(data.id);
	 			$("#area_edit_name").val(data.name);
	 			$("#area_edit_status").val(data.status);
	 		}
	 	});
	});

	//==================== Package Update Script ======================//

	$("#area_update_data").on("submit", function(event){
		event.preventDefault();
		var url = $(this).attr('action');
		var type = $(this).attr('method');
		var data = $(this).serialize();
		$.ajax({
			url : url,
			method: type,
			data: data,
			dataType: "JSON",
			success: function(data){
				$.each(data.errors, function(key, value){
                  $('#area_update_errors').append('<li><span style="color:red;"><b>Error!</b> '+value+'</span></li>');
                 });
				if(data == 'success'){
					$("#area_edit_modal").modal("hide");
					swal("Congrats: Updated!", "Area Update Successfully!", "success");
					return LoadAllArea();
					$("#area_udpate_data").trigger('reset');
				}
			}
		});
	});

	//==================== Area delete view Script ======================//
	
	$(document).on('click', '#area_delete', function(e){
	 	e.preventDefault();
	 	var id = $(this).data('id');
	 	var url = $(this).attr('href');
	 	$.ajax({
	 		url: url,
	 		data: {id:id},
	 		dataTyep: "JSON",
	 		success: function(data){
	 			$("#area_delete_modal").modal("show");
	 			$("#area_delete_id").val(data.id);
	 		}
	 	});
	});

	//==================== Area delete Script ======================//

	$("#area_delete_data").on("submit", function(event){
		event.preventDefault();
		var url = $(this).attr('action');
		var type = $(this).attr('method');
		var data = $(this).serialize();
		$.ajax({
			url : url,
			method: type,
			data: data,
			dataType: "JSON",
			success: function(data){
				if(data == 'success'){
					$("#area_delete_modal").modal("hide");
					swal("Congrats: Deleted!", "Area Deleted Successfully!", "success");
					return LoadAllArea();
					$("#area_delete_data").trigger('reset');
				}
			}
		});
	});
});