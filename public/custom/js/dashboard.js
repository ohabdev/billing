$(document).ready(function(){

	//===============Load All Clients Bill=====================//

	totalClients();
	function totalClients()
	{
		$.ajax({
			url : "dashboard/total_clients",
			method: "GET",
			dataType: "JSON",
			success: function(data)
			{
				if (!data == '') 
				{
					$(".total_clients").text(data.clients);
					$(".activeClients").text(data.activeClients);
					$(".inactiveClients").text(data.inactiveClients);

					var activeClientsPercent = ((data.activeClients*100)/data.clients);
					$(".active_vs_total_clients").attr("style", "width:"+activeClientsPercent+"%");

					var inactiveClientsPercent = ((data.inactiveClients*100)/data.clients);
					$(".inactive_vs_total_clients").attr("style", "width:"+inactiveClientsPercent+"%");
				}else
				{
					$(".total_clients").text('0.00');
					$(".activeClients").text('0.00');
					$(".inactiveClients").text('0.00');
					$(".active_vs_total_clients").attr("style", "width:"+0+"%");
					$(".inactive_vs_total_clients").attr("style", "width:"+0+"%");
				}
			}
		});
	}

	//===============Load Total Bill=====================//
	totalBill();
	function totalBill()
	{
		$.ajax({
			url : "dashboard/total_bill",
			method: "GET",
			dataType: "JSON",
			success: function(data)
			{
				if (!data == '') 
				{
					$(".total_bill").text(data.totalBill);
					$(".total_paid_amt").text(data.totalPaidAmount);
					$(".total_due_amt").text(data.totalDueAmount);

					var paidPercent = ((data.totalPaidAmount*100)/data.totalBill);
					$(".paid_vs_total_month").attr("style", "width:"+paidPercent+"%");

					var duePercent = ((data.totalDueAmount*100)/data.totalBill);
					$(".due_vs_total_month").attr("style", "width:"+duePercent+"%");
				}else
				{
					$(".total_bill").text('0.00');
					$(".total_paid_amt").text('0.00');
					$(".total_due_amt").text('0.00');
					$(".paid_vs_total_month").attr("style", "width:"+0+"%");
					$(".due_vs_total_month").attr("style", "width:"+0+"%");
				}
			}
		});
	}


	//===============Load Month Bill=====================//
	monthlyBill();
	function monthlyBill()
	{
		$.ajax({
			url : "dashboard/monthly_bill",
			method: "GET",
			dataType: "JSON",
			success: function(data)
			{
				if (!data == '') 
				{
					$(".monthly_bill").text(data.monthlyBill);
					$(".monthly_paid").text(data.monthlyPaid);
					$(".monthly_due").text(data.monthlyDue);

					var monthlyPaidPercent = ((data.monthlyPaid*100)/data.monthlyBill);
					$(".monthly_paid_percent").attr("style", "width:"+monthlyPaidPercent+"%");

					var monthlyDuePercent = ((data.monthlyDue*100)/data.monthlyBill);
					$(".monthly_due_percent").attr("style", "width:"+monthlyDuePercent+"%");
				}else
				{
					$(".monthly_bill").text('0.00');
					$(".monthly_paid").text('0.00');
					$(".monthly_due").text('0.00');
					$(".monthly_paid_percent").attr("style", "width:"+0+"%");
					$(".monthly_due_percent").attr("style", "width:"+0+"%");
				}
			}
		});
	}





});