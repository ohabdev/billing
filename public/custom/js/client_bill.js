// $(document).ready(function(){

// 	//============== Client Bill view edit Script =================//
// 	$(document).on('click', '#client_bill_view', function(e){
// 	 	e.preventDefault();
// 	 	var id = $(this).data('id');
// 	 	var url = $(this).attr('href');
// 	 	$.ajax({
// 	 		url: url,
// 	 		data: {id:id},
// 	 		dataType: "JSON",
// 	 		success: function(data){
// 	 			console.log(data);
// 	 			$("#client_bill_modal").modal("show");
// 	 			$(".client_nid").text(data.clients[0].nid);
// 	 			$(".connection_date").text(data.clients[0].connection_date);
// 	 			$(".connection_fee").text(data.clients[0].connection_fee);
// 	 			$(".package").text(data.clients[0].package);
// 	 			$(".monthly_bill").text(data.clients[0].monthly_bill);
// 	 			$(".contact_number").text(data.clients[0].contact_number);
// 	 			$(".area").text(data.clients[0].area);
// 	 			$(".connection_address").text(data.clients[0].connection_address);
// 	 			$(".billing_address").text(data.clients[0].billing_address);
// 	 			if (data.clients[0].status == 1)
// 	 			{
// 	 				$(".client_status").html('<i class="fa fa-check-circle" style="color:green;"></i> Active Profile');
// 	 			}else{
// 	 				$(".client_status").html('<i class="fa fa-close" style="color:red;"></i> Inactive Profile');
// 	 			}
// 	 		}
// 	 	});
// 	});
// });