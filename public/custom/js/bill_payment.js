$(document).ready(function(){


	LoadAllBill();
  	function LoadAllBill()
	{
		$('#billTable').DataTable({
	        "processing": true,
	        "serverside": true,
	        "ajax": "bill_payment/show",
	        "columns": [
	            { "data": "billId"},
	            { "data": "clients_id"},
	            { "data": "year" },
	            { "data": "month" },
	            { "data": "total_amt"},
	            { "data": "paid_amt"},
	            { "data": "due_amt"},
	            { "data": "current_amt"},
	            { "data": "payment_deadline"},
	        ]
	   	});	
  	}


	//============== Bill Create Script =================//

  	$("#client_create_bill").on("submit", function(event){
		event.preventDefault();
		var url = $(this).attr('action');
		var type = $(this).attr('method');
		var formData = new FormData($(this)[0]);
		$.ajax({
			url: url,
	       	type: type,
	       	data: formData,
	       	async: false,
	       	cache: false,
	      	contentType: false,
	       	enctype: 'multipart/form-data',
	       	processData: false,
			success: function(data){
				$.each(data.errors, function(key, value){
                  $('#bill_create_errors').append('<li><span style="color:red;"><b>Error!</b> '+value+'</span></li>');
                 });
				if(data == 'success'){
					swal("Congrats: Generated!", "Client Bill Generated Successfully!", "success");
				}
				return LoadAllBill();
			}
		});
	});

	//==================== Client Bill Info view Script ======================//
	
	$(document).on('click', '#client_bill_view', function(e){
	 	e.preventDefault();
	 	var id = $(this).data('id');
	 	var url = $(this).attr('href');
	 	$.ajax({
	 		url: url,
	 		data: {id:id},
	 		dataTyep: "JSON",
	 		success: function(data){
	 			$("#client_bill_modal").modal("show");
	 			$(".client_nid").text(data.clients[0].nid);
	 			$(".connection_date").text(data.clients[0].connection_date);
	 			$(".connection_fee").text(data.clients[0].connection_fee);
	 			$(".package").text(data.clients[0].package);
	 			$(".monthly_bill").text(data.clients[0].monthly_bill);
	 			$(".contact_number").text(data.clients[0].contact_number);
	 			$(".area").text(data.clients[0].area);
	 			$(".connection_address").text(data.clients[0].connection_address);
	 			$(".billing_address").text(data.clients[0].billing_address);
	 			if (data.clients[0].status == 1)
	 			{
	 				$(".client_status").html('<i class="fa fa-check-circle" style="color:green;"></i> Active Profile');
	 			}else{
	 				$(".client_status").html('<i class="fa fa-close" style="color:red;"></i> Inactive Profile');
	 			}
				$("#show_bill_info").empty();
	 			$.each(data.bills, function(key, bills){
	 				var month;
						switch (bills.month) {
						  case 1:
						    month = "January";
						    break;
						  case 2:
						    month = "February";
						    break;
						  case 3:
						    month = "March";
						    break;
						  case 4:
						    month = "May";
						    break;
						  case 5:
						    month = "April";
						    break;
						  case 6:
						    month = "June";
						    break;
						  case 7:
						    month = "July";
						    break;
						  case  8:
						    month = "August";
						    break;
						  case  9:
						    month = "September";
						    break;
						  case  10:
						    month = "October";
						    break;
						  case  11:
						    month = "November";
						    break;
						  case  12:
						    month = "December";
						    break;
						}
	             	var row = '<tr><td>'+bills.billId+'</td>';
		             	// if (bills.month == 1)
		             	// {
		             	// 	row += '<td>January</td>';
		             	// }else if(bills.month == 2)
		             	// {
		             	// 	row += '<td>February</td>';
		             	// }else if(bills.month == 3)
		             	// {
		             	// 	row += '<td>March</td>';
		             	// }else if(bills.month == 4)
		             	// {
		             	// 	row += '<td>April</td>';
		             	// }else if(bills.month == 5)
		             	// {
		             	// 	row += '<td>May</td>';
		             	// }else if(bills.month == 6)
		             	// {
		             	// 	row += '<td>June</td>';
		             	// }else if(bills.month == 7)
		             	// {
		             	// 	row += '<td>July</td>';
		             	// }else if(bills.month == 8)
		             	// {
		             	// 	row += '<td>August</td>';
		             	// }else if(bills.month == 9)
		             	// {
		             	// 	row += '<td>September</td>';
		             	// }else if(bills.month == 10)
		             	// {
		             	// 	row += '<td>October</td>';
		             	// }else if(bills.month == 11)
		             	// {
		             	// 	row += '<td>December</td>';
		             	// }else if(bills.month == 12)
		             	// {
		             	// 	row += '<td>December</td>';
		             	// }else{
		             	// 	row += '<td>Month not define</td>';
		             	// }
	             		row += '<td>'+bills.year+'</td>';
	             		row += '<td>'+month+'</td>';
	             		if (bills.total_amt == 0.00)
	             		{
	             			row += '<td><span style="color:green;"><i class="fa fa-check-circle"></i> Paid </span></td>';
	             		}else{
	             			row += '<td>'+bills.total_amt+'</td>';
	             		}
	             		row += '<td>'+bills.current_amt+'</td>';
	             		row += '<td>'+bills.paid_amt+'</td>';
	             		if (bills.due_amt == 0.00)
	             		{
	             			row += '<td><span style="color:green;"><i class="fa fa-check-circle"></i> Paid </span></td>';
	             		}else{
	             			row += '<td><span style="color:red;">'+bills.due_amt+' </span></td>';
	             		}

	             		row += '<td>'+bills.payment_deadline+'</td>';
	             	$("#show_bill_info").append(row);		
	            });

             	var row = '<tr><td>'+data.lastBill[0].billId+'</td>';
             		if (data.lastBill[0].month == 1)
	             	{
	             		row += '<td>January</td>';
	             	}else if(data.lastBill[0].month == 2)
	             	{
	             		row += '<td>February</td>';
	             	}else if(data.lastBill[0].month == 3)
	             	{
	             		row += '<td>March</td>';
	             	}else if(data.lastBill[0].month == 4)
	             	{
	             		row += '<td>April</td>';
	             	}else if(data.lastBill[0].month == 5)
	             	{
	             		row += '<td>May</td>';
	             	}else if(data.lastBill[0].month == 6)
	             	{
	             		row += '<td>June</td>';
	             	}else if(data.lastBill[0].month == 7)
	             	{
	             		row += '<td>July</td>';
	             	}else if(data.lastBill[0].month == 8)
	             	{
	             		row += '<td>August</td>';
	             	}else if(data.lastBill[0].month == 9)
	             	{
	             		row += '<td>September</td>';
	             	}else if(data.lastBill[0].month == 10)
	             	{
	             		row += '<td>October</td>';
	             	}else if(data.lastBill[0].month == 11)
	             	{
	             		row += '<td>December</td>';
	             	}else if(data.lastBill[0].month == 12)
	             	{
	             		row += '<td>December</td>';
	             	}else{
	             		row += '<td>Month not define</td>';
	             	}

             		row += '<td>'+data.lastBill[0].year+'</td>';

             		if (data.lastBill[0].total_amt == 0.00)
             		{
             			row += '<td><span style="color:green;"><i class="fa fa-check-circle"></i> Paid </span></td>';
             		}else{
             			row += '<td>'+data.lastBill[0].total_amt+'</td>';
             		}
             		row += '<td>'+data.lastBill[0].current_amt+'</td>';
             		row += '<td>'+data.lastBill[0].paid_amt+'</td>';

             		if (data.lastBill[0].due_amt == 0.00)
             		{
             			row += '<td><span style="color:green;"><i class="fa fa-check-circle"></i> Paid </span></td>';
             		}else{
             			row += '<td><span style="color:green;">'+data.lastBill[0].due_amt+'</span></td>';
             		}

             		row += '<td>'+data.lastBill[0].payment_deadline+'</td>';
             		row += '<td><a href="client/payment_receive" id="payment_receive" data-id="'+data.lastBill[0].clients_id+'" data-toggle="modal" data-target="#payment_receive_modal" class="btn btn-sm btn-success"><i class="fa fa-money"></i> Payment </a></td></tr>';
             	$("#lastBill").html(row);
	 		}
	 	});
	});

    $(document).on('click', '#payment_receive', function(e){
        e.preventDefault();
        var id = $(this).data('id');
 		var url = $(this).attr('href');
 		$.ajax({
 			url: url,
	 		data: {id:id},
	 		dataTyep: "JSON",
	 		success: function(data){
	 			$('.client_name').val(data[0].name);
	 			$('.clientid').val(data[0].id);
	 			$('.billId').val(data[0].billId);
	 			$('.bill_month').val(data[0].month);
	 			$('.bill_year').val(data[0].year);
	 			$('.t_amt').val(data[0].total_amt);
	 			$('.p_amt').val(data[0].paid_amt);
	 			$('.d_amt').val(data[0].due_amt);
	 		}
 		});
		$('#client_bill_modal').modal('hide').on('hidden.bs.modal', function (e) {
	        $('#payment_receive_modal').modal('show');
	        $(this).off('hidden.bs.modal');
    	});
    });


    //==================== Payment Calculation ======================//

    $("#bill_paid_amt").keyup(function(){
	  $('#bill_paid_amt').each(function() {
	  	var totalAmt = $("#totalAmt").val();
	  	var paid = $("#bill_paid_amt").val();
	  	var sum = totalAmt - paid;
	        $(".current_due_amt").val(sum);
	    });
	});

    //==================== Payment Update ======================//

    $("#payment_update_data").on("submit", function(event){
    	event.preventDefault();
    	var url = $(this).attr("action");
    	var type = $(this).attr("method");
    	var data = $(this).serialize();

    	$.ajax({
    		url : url,
    		method: type,
    		data : data,
    		success: function(data)
    		{
    			if (data == "success") 
    			{
    				swal("Congrats: Received!", "Client Bill Received Successfully!", "success");
    			}
    			$("#payment_receive_modal").modal("hide");
    		}
    	});
    });




});