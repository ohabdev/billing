$(document).ready(function(){

	//==============Load ADD Patient Form Data=================//

	  // var base_URL = 'http://localhost/billing/storage/public/app/';
	  var base_URL = 'http://localhost/billing/storage/app/public/';

  	LoadClientFormData();
 	function LoadClientFormData(){
  		$.ajax({
			url: 'client/form_data',
			method: "GET",
			success: function(data){
				$.each(data.package, function(key, package){
	               $('.packageIdName').append('<option value="'+package.id+'">'+package.name+'</option>');
	            });
				$.each(data.area, function(key, area){
	               $('.areaIdName').append('<option value="'+area.id+'">'+area.name+'</option>');
	            });
			}	
		});
  	}

  	//==================== Load Client Script ======================//

  	var AllClients = $('#clientTable').DataTable({
	        "processing": true,
	        "serverside": true,
	        ajax: 'client/show',
	        "columns": [
	            { "data": "id"},
	            { "data": "client_id"},
	            { "data": "name" },
	            { 
	            	"data": "image",
	            	"render" : function (data, type, JsonResultRow, meta) 
	            	{
						return '<img src="'+base_URL+'image/'+JsonResultRow.image+'" width="50" height="40" style="border-radius: 50%;"">';
					}
	            },
	            { "data": "area" },
	            { "data": "billing_address" },
	            { "data": "contact_number" },
	            { "data": "package" },
	            { "data": "monthly_bill" },
	            { 
	            	"data": 'status', 
	              	"render": function(data)
	              	{ 
	                	if(data == 1)
	                	{
			                return '<span style="color:green;"><i class="fa fa-check-circle" style="color:green;"></i> Active</span>'; 
			            }else 
			           	{
			                return '<span style="color:red;"><i class="fa fa-close"></i> Inactive</span>';
			            }
	              	}
	            },
	            { 
	            	"render" : function (data, type, JsonResultRow, meta) {
	            			var row_data = '';
							row_data += '<div class="dropdown">';
							row_data += '<a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">';
							row_data += '<i class="fa fa-ellipsis-h"></i></a>';
							row_data += '<div class="dropdown-menu dropdown-menu-right">';
							row_data += '<a class="dropdown-item" href="client/view" id="client_view" data-id="'+JsonResultRow.id+'" data-toggle="modal" data-target="#client_view_modal"><i class="fa fa-eye"></i> View</a>';
							row_data += '<a class="dropdown-item" href="client/edit" id="client_edit" data-id="'+JsonResultRow.id+'" data-toggle="modal" data-target="#client_edit_modal"><i class="fa fa-pencil"></i> Edit</a>';
							row_data += '<a class="dropdown-item" href="client/delete_view" id="client_delete" data-id="'+JsonResultRow.id+'" data-toggle="modal" data-target="#client_delete_modal"><i class="fa fa-trash"></i>Delete</a>';
							row_data += '</div></div>';
							return row_data;
							// return '<img src="'+base_URL+'image/'+JsonResultRow.id+'" width="50" height="40" style="border-radius: 50%;"">';
							}
	             },
	            {
	            	"render" : function (data, type, JsonResultRow, meta)
	            	{
						return '<a href="client/view" id="client_bill_view" data-id="'+JsonResultRow.id+'" class="btn btn-sm btn-info" data-toggle="modal" data-target="#client_bill_modal"><i class="fa fa-eye"></i> Bill Info </a>';
					}
	            }
	        ],
	        "order": [[0, "desc"]],
	   	});
  	// var AllClients = LoadAllClient();
 //  	function LoadAllClient()
	// {
	// 	return $('#clientTable').DataTable({
	//         "processing": true,
	//         "serverside": true,
	//         "ajax": 'client/show',
	//         "columns": [
	//             { "data": "id"},
	//             { "data": "client_id"},
	//             { "data": "name" },
	//             { 
	//             	"data": "image",
	//             	"render" : function (data, type, JsonResultRow, meta) 
	//             	{
	// 					return '<img src="'+base_URL+'image/'+JsonResultRow.image+'" width="50" height="40" style="border-radius: 50%;"">';
	// 				}
	//             },
	//             { "data": "area" },
	//             { "data": "billing_address" },
	//             { "data": "contact_number" },
	//             { "data": "package" },
	//             { "data": "monthly_bill" },
	//             { 
	//             	"data": 'status', 
	//               	"render": function(data)
	//               	{ 
	//                 	if(data == 1)
	//                 	{
	// 		                return '<span style="color:green;"><i class="fa fa-check-circle" style="color:green;"></i> Active</span>'; 
	// 		            }else 
	// 		           	{
	// 		                return '<span style="color:red;"><i class="fa fa-close"></i> Inactive</span>';
	// 		            }
	//               	}
	//             },
	//             { 
	//             	"render" : function (data, type, JsonResultRow, meta) {
	//             			var row_data = '';
	// 						row_data += '<div class="dropdown">';
	// 						row_data += '<a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">';
	// 						row_data += '<i class="fa fa-ellipsis-h"></i></a>';
	// 						row_data += '<div class="dropdown-menu dropdown-menu-right">';
	// 						row_data += '<a class="dropdown-item" href="client/view" id="client_view" data-id="'+JsonResultRow.id+'" data-toggle="modal" data-target="#client_view_modal"><i class="fa fa-eye"></i> View</a>';
	// 						row_data += '<a class="dropdown-item" href="client/edit" id="client_edit" data-id="'+JsonResultRow.id+'" data-toggle="modal" data-target="#client_edit_modal"><i class="fa fa-pencil"></i> Edit</a>';
	// 						row_data += '<a class="dropdown-item" href="client/delete_view" id="client_delete" data-id="'+JsonResultRow.id+'" data-toggle="modal" data-target="#client_delete_modal"><i class="fa fa-trash"></i>Delete</a>';
	// 						row_data += '</div></div>';
	// 						return row_data;
	// 						// return '<img src="'+base_URL+'image/'+JsonResultRow.id+'" width="50" height="40" style="border-radius: 50%;"">';
	// 						}
	//              },
	//             {
	//             	"render" : function (data, type, JsonResultRow, meta)
	//             	{
	// 					return '<a href="client/view" id="client_bill_view" data-id="'+JsonResultRow.id+'" class="btn btn-sm btn-info" data-toggle="modal" data-target="#client_bill_modal"><i class="fa fa-eye"></i> Bill Info </a>';
	// 				}
	//             }
	//         ],
	//         "order": [[ 'id', 'desc' ]],
	//    	});	
 //  	}


  	//============== Add Client Script =================//

  	$("#client_add_data").on("submit", function(event){
		event.preventDefault();
		var url = $(this).attr('action');
		var type = $(this).attr('method');
		var formData = new FormData($(this)[0]);
		$.ajax({
			url: url,
	       	type: type,
	       	data: formData,
	       	async: false,
	       	cache: false,
	      	contentType: false,
	       	enctype: 'multipart/form-data',
	       	processData: false,
			success: function(data){
				// console.log(AllClients,data);
				$.each(data.errors, function(key, value){
                  $('#client_add_errors').append('<li><span style="color:red;"><b>Error!</b> '+value+'</span></li>');
                 });
				if(data == 'success'){
					AllClients.ajax.reload();
					swal("Congrats: Saved!", "New Client Added Successfully!", "success");
				}
				$('#client_add_data')[0].reset();
				$('#client_add_modal').modal('hide');
			}
		});
		return false;
	});

	//============== Client View Script =================//

	$(document).on('click', '#client_view', function(e){
	 	e.preventDefault();
	 	var id = $(this).data('id');
	 	var url = $(this).attr('href');
	 	$.ajax({
	 		url: url,
	 		data: {id:id},
	 		dataTyep: "JSON",
	 		success: function(data){
	 			$("#client_view_modal").modal("show");
	 			$(".view_client_id").text(data.clients[0].client_id);
	 			$(".view_nid_number").text(data.clients[0].nid);
	 			$(".view_client_name").text(data.clients[0].name);
	 			$(".view_client_photo").attr('src', base_URL +'image/'+data.clients[0].image);
	 			$(".view_area").text(data.clients[0].area);
	 			$(".view_area_code").text(data.clients[0].area_code);
	 			$(".view_connection_date").text(data.clients[0].connection_date);
	 			$(".view_connection_fee").text(data.clients[0].connection_fee);
	 			$(".view_contact_number").text(data.clients[0].contact_number);
	 			$(".view_connection_address").text(data.clients[0].connection_address);
	 			$(".view_package").text(data.clients[0].package);
	 			$(".view_monthly_bill").text(data.clients[0].monthly_bill);
	 		}
	 	});
	});
	
	//============== Client edit Script =================//

	$(document).on('click', '#client_edit', function(e){
	 	e.preventDefault();
	 	var id = $(this).data('id');
	 	var url = $(this).attr('href');
	 	$.ajax({
	 		url: url,
	 		data: {id:id},
	 		dataTyep: "JSON",
	 		success: function(data){
	 			$("#client_edit_modal").modal("show");
	 			$(".clientID").val(data[0].id);
	 			$(".client_id").val(data[0].client_id);
	 			$(".nid_number").val(data[0].nid);
	 			$(".client_name").val(data[0].name);
	 			$("#client_photo").attr('src', base_URL +'image/'+data[0].image);
	 			$("#areas_id").val(data[0].areas_id);
	 			$(".area_code").val(data[0].area_code);
	 			$(".connection_date").val(data[0].connection_date);
	 			$(".connection_fee").val(data[0].connection_fee);
	 			$(".contact_number").val(data[0].contact_number);
	 			$(".connection_address").val(data[0].connection_address);
	 			$(".billing_address").val(data[0].billing_address);
	 			$("#packages_id").val(data[0].packages_id);
	 			$(".monthly_bill").val(data[0].monthly_bill);
	 			$(".client_status").val(data[0].status);
	 		}
	 	});
	});

	//==================== Client Update Script ======================//

	$("#client_update_data").on("submit", function(event){
		event.preventDefault();
		var url = $(this).attr('action');
		var type = $(this).attr('method');
		var formData = new FormData($(this)[0]);
		$.ajax({
			url: url,
	       	type: type,
	       	data: formData,
	       	async: false,
	       	cache: false,
	      	contentType: false,
	       	enctype: 'multipart/form-data',
	       	processData: false,
			success: function(data){
				$.each(data.errors, function(key, value){
                  $('#client_update_errors').append('<li><span style="color:red;"><b>Error!</b> '+value+'</span></li>');
                 });
				if(data == 'success'){
					$("#client_edit_modal").modal("hide");
					swal("Congrats: Updated!", "Client Data Updated Successfully!", "success");
					return LoadAllClient();
				}
			}
		});
	});

	//==================== Client Delete view Script ======================//
	
	$(document).on('click', '#client_delete', function(e){
	 	e.preventDefault();
	 	var id = $(this).data('id');
	 	var url = $(this).attr('href');
	 	$.ajax({
	 		url: url,
	 		data: {id:id},
	 		dataTyep: "JSON",
	 		success: function(data){
	 			$("#client_delete_modal").modal("show");
	 			$("#client_delete_id").val(data.id);
	 		}
	 	});
	});

	//==================== Client Delete  Script ======================//

	$("#client_delete_data").on("submit", function(event){
		event.preventDefault();
		var url = $(this).attr('action');
		var type = $(this).attr('method');
		var data = $(this).serialize();
		$.ajax({
			url : url,
			method: type,
			data: data,
			dataType: "JSON",
			success: function(data){
				if(data == 'success'){
					$("#client_delete_modal").modal("hide");
					swal("Congrats: Deleted!", "Client Deleted Successfully!", "success");
					return LoadAllClient();
				}
			}
		});
	});
});

