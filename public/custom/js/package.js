$(document).ready(function(){

  	//=========== Add Packages Data Script======================
  	
  	$("#package_add_data").on("submit", function(event){
		event.preventDefault();
		var url = $(this).attr('action');
		var type = $(this).attr('method');
		var formData = new FormData($(this)[0]);
		$.ajax({
			url: url,
	       	type: type,
	       	data: formData,
	       	async: false,
	       	cache: false,
	      	contentType: false,
	       	enctype: 'multipart/form-data',
	       	processData: false,
			success: function(data){
				$.each(data.errors, function(key, value){
                  $('#package_add_errors').append('<li><span style="color:red;"><b>Error!</b> '+value+'</span></li>');
                 });
				if(data == 'success')
				{
					swal("Congrats: Saved!", "New Package Added Successfully!", "success");
				}
				$("#package_add_data").trigger('reset');
				return LoadAllPackages();
			}
		});
	});

  	//==================== Load All Package Script======================//
  	
	LoadAllPackages();

	function LoadAllPackages(){
		$.ajax({
			url: 'package/show',
			method: "GET",
			success: function(data){
				var row_data = '';
				var sl = 1;
				var i = 0;
				for(i=0; i<data.length; i++)
				{
					row_data += '<tr><td>'+ sl++ +'</td>';
					row_data += '<td>'+data[i].name+'</td>';
					if (data[i].status == 1)
					{
						row_data += '<td><i class="fa fa-check-circle" style="color:green"></i></td>';
					}else{
						row_data += '<td><i class="fa fa-close" style="color:red"></i></td>';
					}
					row_data += '<td>';
					row_data += '<a href="package/edit" id="package_edit" data-id="'+data[i].id+'" data-toggle="modal" data-target="#package_edit_modal" style="color:#17a2b8; margin:5px;"><i class="fa fa-edit"></i></a>';
					row_data += '<a href="package/delete_view" id="package_delete" data-id="'+data[i].id+'" data-toggle="modal" data-target="#package_delete_modal" style="color:red; margin:5px;"><i class="fa fa-trash"></i></a>';
					row_data += '</td>';
					row_data += '</tr>';
				}
				$("#show_all_packages").html(row_data);
			}	
		});
	}

	//==================== Package Edit Script ======================//
	
	$(document).on('click', '#package_edit', function(e){
	 	e.preventDefault();
	 	var id = $(this).data('id');
	 	var url = $(this).attr('href');
	 	$.ajax({
	 		url: url,
	 		data: {id:id},
	 		dataTyep: "JSON",
	 		success: function(data){
	 			$("#package_edit_modal").modal("show");
	 			$("#package_edit_id").val(data.id);
	 			$("#package_edit_name").val(data.name);
	 			$("#package_edit_status").val(data.status);
	 		}
	 	});
	});

	//==================== Package Update Script ======================//

	$("#package_update_data").on("submit", function(event){
		event.preventDefault();
		var url = $(this).attr('action');
		var type = $(this).attr('method');
		var data = $(this).serialize();
		$.ajax({
			url : url,
			method: type,
			data: data,
			dataType: "JSON",
			success: function(data){
				$.each(data.errors, function(key, value){
                  $('#package_update_errors').append('<li><span style="color:red;"><b>Error!</b> '+value+'</span></li>');
                 });
				if(data == 'success'){
					$("#package_edit_modal").modal("hide");
					swal("Congrats: Updated!", "Package Update Successfully!", "success");
					return LoadAllPackages();
					$("#package_udpate_data").trigger('reset');
				}
			}
		});
	});

	//==================== Package delete Script ======================//
	
	$(document).on('click', '#package_delete', function(e){
	 	e.preventDefault();
	 	var id = $(this).data('id');
	 	var url = $(this).attr('href');
	 	$.ajax({
	 		url: url,
	 		data: {id:id},
	 		dataTyep: "JSON",
	 		success: function(data){
	 			$("#package_delete_modal").modal("show");
	 			$("#package_delete_id").val(data.id);
	 		}
	 	});
	});

	//==================== Package Update Script ======================//

	$("#package_delete_data").on("submit", function(event){
		event.preventDefault();
		var url = $(this).attr('action');
		var type = $(this).attr('method');
		var data = $(this).serialize();
		$.ajax({
			url : url,
			method: type,
			data: data,
			dataType: "JSON",
			success: function(data){
				if(data == 'success'){
					$("#package_delete_modal").modal("hide");
					swal("Congrats: Deleted!", "Package Deleted Successfully!", "success");
					return LoadAllPackages();
					$("#package_delete_data").trigger('reset');
				}
			}
		});
	});

});