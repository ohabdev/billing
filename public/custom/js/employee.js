$(document).ready(function(){

	  var base_URL = 'http://localhost/billing/storage/app/public/image/';

  	//==================== Load Employee Data======================//
  	
	LoadAllEmployee();

	function LoadAllEmployee(){
		$.ajax({
			url: 'employee/show',
			method: "GET",
			success: function(data){
				var row_data = '';
				var sl = 1;
				var i = 0;
				for(i=0; i<data.length; i++)
				{
					row_data += '<tr>'; 
					row_data += '<td>'+ sl++ +'</td>';
					row_data += '<td>'+data[i].name+'</td>';
					row_data += '<td><img src="'+base_URL+'employee/'+data[i].image+'" width="50" height="40" style="border-radius: 50%;""></td>';
					row_data += '<td>'+data[i].phone_number+'</td>';
					row_data += '<td>'+data[i].address+'</td>';
					row_data += '<td>'+data[i].hire_date+'</td>';
					row_data += '<td>';
					row_data += '<div class="dropdown">';
					row_data += '<a class="btn btn-outline-primary dropdown-toggle" role="button" data-toggle="dropdown">';
					row_data += '<i class="fa fa-ellipsis-h"></i></a>';
					row_data += '<div class="dropdown-menu dropdown-menu-right">';
					row_data += '<a class="dropdown-item" href="employee/view" id="employee_view" data-id="'+data[i].id+'" data-toggle="modal" data-target="#employee_view_modal"><i class="fa fa-eye"></i> View</a>';
					row_data += '<a class="dropdown-item" href="employee/edit" id="employee_edit" data-id="'+data[i].id+'" data-toggle="modal" data-target="#employee_edit_modal"><i class="fa fa-pencil"></i> Edit</a>';
					row_data += '<a class="dropdown-item" href="employee/delete_view" id="employee_delete" data-id="'+data[i].id+'" data-toggle="modal" data-target="#employee_delete_modal"><i class="fa fa-trash"></i>Delete</a>';
					row_data += '</div></div>';
					row_data += '</td>';
					row_data += '</tr>';
				}
				$("#show_all_employee").html(row_data);
			}	
		});
	}

  	//============== Add Employee Script =================//

  	$("#employee_add_data").on("submit", function(e){
		e.preventDefault();
		var url = $(this).attr('action');
		var type = $(this).attr('method');
		var formData = new FormData($(this)[0]);
		$.ajax({
			url: url,
	       	type: type,
	       	data: formData,
	       	async: false,
	       	cache: false,
	      	contentType: false,
	       	enctype: 'multipart/form-data',
	       	processData: false,
			success: function(data){
				$.each(data.errors, function(key, value){
                  $('.employee_add_errors').append('<li><span style="color:red;"><b>Error!</b> '+value+'</span></li>');
                 });
				if(data == 'success'){
					swal("Congrats: Saved!", "New Employee Added Successfully!", "success");
					$('#employee_add_modal').modal('hide');
				}
				return LoadAllEmployee();
			}
		});
	});

	//============== Employee View Script =================//

	$(document).on('click', '#employee_view', function(e){
	 	e.preventDefault();
	 	var id = $(this).data('id');
	 	var url = $(this).attr('href');
	 	$.ajax({
	 		url: url,
	 		data: {id:id},
	 		dataTyep: "JSON",
	 		success: function(data){
	 			console.log(data);
	 			$("#employee_view_modal").modal("show");
	 			$(".view_employee_name").text(data[0].name);
	 			$(".view_employee_phone_number").text(data[0].phone_number);
	 			$(".view_employee_hire_date").text(data[0].hire_date);
	 			$(".view_employee_image").attr('src', base_URL +'employee/'+data[0].image);
	 			$(".view_employee_address").text(data[0].address);
	 		}
	 	});
	});
	//============== Employee edit Script =================//

	$(document).on('click', '#employee_edit', function(e){
	 	e.preventDefault();
	 	var id = $(this).data('id');
	 	var url = $(this).attr('href');
	 	$.ajax({
	 		url: url,
	 		data: {id:id},
	 		dataTyep: "JSON",
	 		success: function(data){
	 			console.log(data);
	 			$("#employee_edit_modal").modal("show");
	 			$(".edit_employee_id").val(data[0].id);
	 			$(".edit_employee_name").val(data[0].name);
	 			$(".edit_employee_phone_number").val(data[0].phone_number);
	 			$(".edit_employee_password").val(data[0].password);
	 			$(".edit_employee_hire_date").val(data[0].hire_date);
	 			$("#edit_employee_image").attr('src', base_URL +'employee/'+data[0].image);
	 			$(".edit_employee_address").val(data[0].address);
	 		}
	 	});
	});

	//==================== Employee Update Script ======================//

	$("#employee_update_data").on("submit", function(event){
		event.preventDefault();
		var url = $(this).attr('action');
		var type = $(this).attr('method');
		var formData = new FormData($(this)[0]);
		$.ajax({
			url: url,
	       	type: type,
	       	data: formData,
	       	async: false,
	       	cache: false,
	      	contentType: false,
	       	enctype: 'multipart/form-data',
	       	processData: false,
			success: function(data){
				$.each(data.errors, function(key, value){
                  $('.employee_update_errors').append('<li><span style="color:red;"><b>Error!</b> '+value+'</span></li>');
                 });
				if(data == 'success'){
					$("#employee_edit_modal").modal("hide");
					swal("Congrats: Updated!", "Employee Data Updated Successfully!", "success");
					return LoadAllEmployee();
				}
			}
		});
	});

	//==================== Employee delete view Script ======================//
	
	$(document).on('click', '#employee_delete', function(e){
	 	e.preventDefault();
	 	var id = $(this).data('id');
	 	var url = $(this).attr('href');
	 	$.ajax({
	 		url: url,
	 		data: {id:id},
	 		dataTyep: "JSON",
	 		success: function(data){
	 			$("#employee_delete_modal").modal("show");
	 			$("#employee_delete_id").val(data.id);
	 		}
	 	});
	});

	//==================== Employee delete Script ======================//

	$("#employee_delete_data").on("submit", function(event){
		event.preventDefault();
		var url = $(this).attr('action');
		var type = $(this).attr('method');
		var data = $(this).serialize();
		$.ajax({
			url : url,
			method: type,
			data: data,
			dataType: "JSON",
			success: function(data){
				if(data == 'success'){
					$("#employee_delete_modal").modal("hide");
					swal("Congrats: Deleted!", "Employee Deleted Successfully!", "success");
					return LoadAllEmployee();
				}
			}
		});
	});




});
