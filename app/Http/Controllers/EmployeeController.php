<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Employee;
use Validator;

class EmployeeController extends Controller
    {
        public function index()
        {
            return view('dashboard.employee.index');
            
        }

        public function store(Request $request)
        {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'hire_date' => 'required',
                'phone_number' => 'required|numeric',
                'password' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10000',
                'address' => 'required',
            ]);

            if ($validator->fails())
            {
               return response()->json(['errors'=>$validator->errors()->all()]);
            }

            if ($validator->passes()) {

                $image = $request->file('image');
                if (isset($image))
                {
                    $currentDate = Carbon::now()->toDateString();
                    $imageName = 'employee'.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
                    if (!Storage::disk('public')->exists('image/employee'))
                    {
                        Storage::disk('public')->makeDirectory('image/employee');
                    }
                    $imagePath = Image::make($image)->resize(150,150)->stream();
                    Storage::disk('public')->put('image/employee/'.$imageName,$imagePath);
                }else{
                    $imageName = "default_employee_img.png";
                }

                $data = Employee::insert([
                    "name" => $request->name,
                    "hire_date" => $request->hire_date,
                    "phone_number" => $request->phone_number,
                    "password" => $request->password,
                    "image" => $imageName,
                    "address" => $request->address,
                    "created_at" => Carbon::now()
                ]);

                if($data)
                {
                    return response()->json("success");
                }
            }
        }

        public function show()
        {
            $data = DB::table('employees')->orderBy('id', 'DESC')->get();
            return response()->json($data);
        }

        public function view(Request $request)
        {
            $id = $request->id;
            $data = DB::table('employees')->where('id', $id)->get();
            return response()->json($data);
        }
                

        public function edit(Request $request)
        {
            $id = $request->id;
            $data = DB::table('employees')->where('id', $id)->get();
            return response()->json($data);
        }

        public function update(Request $request)
        {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'hire_date' => 'required',
                'phone_number' => 'required|numeric',
                'password' => 'required|numeric|min:10',
                //'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10000',
                'address' => 'required',
            ]);
            
            if ($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }

            if ($validator->passes()) {
                $image = $request->file('image');
                $employee = Employee::find($request->id);
                if (isset($image))
                {
                    $currentDate = Carbon::now()->toDateString();
                    $imageName = 'employee'.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
                    if (Storage::disk('public')->exists('image/employee/'.$employee->image))
                    {
                        Storage::disk('public')->delete('image/employee/'.$employee->image);
                    }
                    $imagePath = Image::make($image)->resize(150,150)->stream();
                    Storage::disk('public')->put('image/employee/'.$imageName,$imagePath);
                }else{
                    $imageName = $employee->image;
                }

                $data = Employee::where('id', $request->id)->update([
                    "name" => $request->name,
                    "hire_date" => $request->hire_date,
                    "phone_number" => $request->phone_number,
                    "password" => $request->password,
                    "image" => $imageName,
                    "address" => $request->address,
                    "updated_at" => Carbon::now(),
                ]);
                if($data)
                {
                    return response()->json("success");
                }else{
                    return response()->json("query_fail");
                }
            }
        }

        public function delete_view(Request $request)
        {
            $id = $request->id;
            $data = Employee::find($id);
            return response()->json($data);
        }

        public function delete(Request $request)
        {
            $id = $request->id;
            $employee = Employee::find($id);

            if (Storage::disk('public')->exists('image/employee/'.$employee->image))
            {
                Storage::disk('public')->delete('image/employee/'.$employee->image);
            }
            $employee->delete();
            if($employee)
            {
                return response()->json("success");
            }
        }

}

