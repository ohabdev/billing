<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Expense;
use Validator;

class ExpenseController extends Controller
{
	public function index()
    {
    	return view('dashboard.expense.index');
    } 

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:expenses',
        ]);
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        if ($validator->passes()) {
            $data = Expense::insert([
                "name" => $request->name,
                "created_at" => Carbon::now(),
            ]);
            if($data)
            {
                return response()->json("success");
            }
        }
    }

    public function show()
    {
        $data = Expense::orderBy('id', 'DESC')->get();
        return response()->json($data);
    }

    public function edit(Request $request)
    {
        $id = $request->id;
        $data = Expense::find($id);
        return response()->json($data);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        if ($validator->passes()) {
            $data = Expense::where('id', $request->id)->update([
                "name" => $request->name,
                "status" => $request->status,
                "updated_at" => Carbon::now(),
            ]);
            if($data)
            {
                return response()->json("success");
            }
        }
    }

    public function delete_view(Request $request)
    {
        $id = $request->id;
        $data = Expense::find($id);
        return response()->json($data);
    }
    public function delete(Request $request)
    {
        $id = $request->id;
        $data = Expense::find($id)->delete();
        if($data)
        {
            return response()->json("success");
        }
    }


}
