<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\BillPayment;
use App\ClientBill;
use Carbon\Carbon;
use App\Package;
use App\Client;
use App\Area;
use Validator;

class BillPaymentController extends Controller
{
    public function index()
    {
        return view('dashboard.bills_payment.create_bill');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'month' => 'required|numeric|unique:bill_payments',
            'year' => 'required|numeric',
            'payment_deadline' => 'required',
        ]);


        if ($validator->fails())
        {
           return response()->json(['errors'=>$validator->errors()->all()]);
        }

        if ($validator->passes()) {

        	$clients = DB::table('clients')->get();
        	$i = 0;
        	for ($i=0; $i < count($clients); $i++) {
        		$id = $clients[$i]->id;
        		$bills = DB::table('bill_payments')->where('clients_id', $id)->orderBy('id', "desc")->take(1)->get();
        		$total_amt = $bills[0]->total_amt + $bills[0]->current_amt;
        		$this_month = $bills[0]->current_amt;
        		$due = $total_amt - $bills[0]->paid_amt;
        		$BillID = rand(10000,99999);
        		$data = BillPayment::insert([
	            		"clients_id" => $clients[$i]->id,
		                "month" => $request->month,
		                "year" => $request->year,
		                "billId" => $BillID,
		                "total_amt" =>  $total_amt,
		                "current_amt" =>  $this_month,
		                "paid_amt" =>  0.00,
		                "due_amt" => $due,
		                "payment_deadline" => $request->payment_deadline,
		                "created_at" => Carbon::now(),
	            ]);
        	}

            if($data)
            {
                return response()->json("success");
            }
        }
    }

    public function show()
    {
        $data = DB::table('bill_payments')->get();
        return response()->json([
                "draw" => 1,
                "recordsTotal" => 57,
                "recordsFiltered" => 57,
                "data" => $data,
        ]);
    }

   public function update(Request $request)
   {

        $data = BillPayment::where('billId', $request->billId)->update([
            "total_amt" => $request->due_amt,
            "paid_amt" => $request->paid_amt,
            "due_amt" => $request->due_amt,
            "updated_at" => Carbon::now(),
        ]);
        return $request;
        if($data)
        {
            return response()->json("success");
        }else{
            return response()->json("query_fail");
        }
   }




}

