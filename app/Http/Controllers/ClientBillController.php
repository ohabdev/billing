<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Package;
use App\ClientBill;
use App\Client;
use App\Area;
use Validator;

class ClientBillController extends Controller
    {
        public function index(Request $request)
        {
            return view('dashboard.client.client_bill_info');
        }
        public function form_data()
        {
            $package = Package::all();
            $area = Area::all();
            return response()->json([
                'package' => $package,
                'area' => $area,
            ]);
        }

        public function store(Request $request)
        {
            $validator = Validator::make($request->all(), [
                'client_id' => 'required',
                //'nid' => 'required',
                'name' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10000',
                'areas_id' => 'required|numeric',
                //'area_code' => 'required',
                'billing_address' => 'required',
                'contact_number' => 'required|numeric|min:10',
                'connection_address' => 'required',
                'connection_date' => 'required',
                'connection_fee' => 'required',
                'packages_id' => 'required|numeric',
                'monthly_bill' => 'required',
            ]);

            if ($validator->fails())
            {
               return response()->json(['errors'=>$validator->errors()->all()]);
            }

            if ($validator->passes()) {
                $image = $request->file('image');
                if (isset($image))
                {
                    $currentDate = Carbon::now()->toDateString();
                    $imageName = 'client-'.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
                    if (!Storage::disk('public')->exists('image'))
                    {
                        Storage::disk('public')->makeDirectory('image');
                    }
                    $imagePath = Image::make($image)->resize(150,150)->stream();
                    Storage::disk('public')->put('image/'.$imageName,$imagePath);
                } else {
                    $imageName = "default_client_img.png";
                }
                $data = Client::insert([
                    "client_id" => $request->client_id,
                    "nid" => $request->nid,
                    "name" => $request->name,
                    "image" => $imageName,
                    "areas_id" => $request->areas_id,
                    "area_code" => $request->area_code,
                    "billing_address" => $request->billing_address,
                    "contact_number" => $request->contact_number,
                    "connection_date" => $request->connection_date,
                    "connection_address" => $request->connection_address,
                    "connection_fee" => $request->connection_fee,
                    "packages_id" => $request->packages_id,
                    "monthly_bill" => $request->monthly_bill,
                    "created_at" => Carbon::now()
                ]);
                if($data)
                {
                    return response()->json("success");
                }
            }
        }

        public function show()
        {
            $data = DB::table('clients')
                ->join('packages', 'clients.packages_id', '=', 'packages.id')
                ->join('areas', 'clients.areas_id', '=', 'areas.id')
                ->select('clients.*', 'packages.name as package', 'areas.name as area')
                ->orderBy('id', 'DESC')->get();
            return response()->json($data);
        }
        public function view(Request $request)
        {
            $id = $request->id;
            $bills = DB::table('bill_payments')->where('clients_id', $id)->orderBy('id', "desc")->take(1)->get();
            $clients = DB::table('clients')
                ->join('packages', 'clients.packages_id', '=', 'packages.id')
                ->join('areas', 'clients.areas_id', '=', 'areas.id')
                ->select('clients.*', 'packages.name as package', 'areas.name as area')
                ->where('clients.id', $id)->get();
            return response()->json([
                "clients" => $clients;
                "bills" => $bills;
            ]);
        }
                

        public function edit(Request $request)
        {
            $id = $request->id;
            $data = DB::table('clients')
                ->join('packages', 'clients.packages_id', '=', 'packages.id')
                ->join('areas', 'clients.areas_id', '=', 'areas.id')
                ->select('clients.*', 'packages.name as package', 'areas.name as area')
                ->where('clients.id', $id)->get();
            return response()->json($data);
        }

        public function update(Request $request)
        {
            $validator = Validator::make($request->all(), [
                'client_id' => 'required',
                //'nid' => 'required',
                'name' => 'required',
                //'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10000',
                'areas_id' => 'required|numeric',
                'area_code' => 'required',
                'billing_address' => 'required',
                'contact_number' => 'required|numeric|min:10',
                'connection_address' => 'required',
                'connection_date' => 'required',
                'connection_fee' => 'required',
                'packages_id' => 'required|numeric',
                'monthly_bill' => 'required',
            ]);
            
            if ($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }
            if ($validator->passes()) {
                $image = $request->file('image');
                $client = Client::find($request->id);
                if (isset($image))
                {
                    $currentDate = Carbon::now()->toDateString();
                    $imageName = 'image-'.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
                    if (Storage::disk('public')->exists('image/'.$client->image))
                    {
                        Storage::disk('public')->delete('image/'.$client->image);
                    }
                    $imagePath = Image::make($image)->resize(150,150)->stream();
                    Storage::disk('public')->put('image/'.$imageName,$imagePath);
                } else {
                    $imageName = $client->image;
                }
                $data = Client::where('id', $request->id)->update([
                    "client_id" => $request->client_id,
                    "nid" => $request->nid,
                    "name" => $request->name,
                    "image" => $imageName,
                    "areas_id" => $request->areas_id,
                    "area_code" => $request->area_code,
                    "billing_address" => $request->billing_address,
                    "contact_number" => $request->contact_number,
                    "connection_address" => $request->connection_address,
                    "connection_date" => $request->connection_date,
                    "connection_fee" => $request->connection_fee,
                    "packages_id" => $request->packages_id,
                    "monthly_bill" => $request->monthly_bill,
                    "status" => $request->status,
                    "updated_at" => Carbon::now(),
                ]);
                if($data)
                {
                    return response()->json("success");
                }else{
                    return response()->json("query_fail");
                }
            }
        }

        public function delete_view(Request $request)
        {
            $id = $request->id;
            $data = Client::find($id);
            return response()->json($data);
        }

        public function delete(Request $request)
        {
            $id = $request->id;
            $client = Client::find($id);
            if (Storage::disk('public')->exists('image/'.$client->image))
            {
                Storage::disk('public')->delete('image/'.$client->image);
            }
            $client->delete();
            if($client)
            {
                return response()->json("success");
            }
        }
}

