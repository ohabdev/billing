<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\BillPayment;
use App\ClientBill;
use Carbon\Carbon;
use App\Package;
use App\Client;
use App\Area;
use Validator;

class ClientController extends Controller
    {
        public function index()
        {
            return view('dashboard.client.index');
        }
        public function form_data()
        {
            $package = Package::all();
            $area = Area::all();
            return response()->json([
                'package' => $package,
                'area' => $area,
            ]);
        }

        public function store(Request $request)
        {
            $validator = Validator::make($request->all(), [
                'client_id' => 'required',
                //'nid' => 'required',
                'name' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10000',
                'areas_id' => 'required|numeric',
                //'area_code' => 'required',
                'billing_address' => 'required',
                'contact_number' => 'required|numeric|min:10',
                'connection_address' => 'required',
                'connection_date' => 'required',
                'connection_fee' => 'required',
                'packages_id' => 'required|numeric',
                'monthly_bill' => 'required',
            ]);

            if ($validator->fails())
            {
               return response()->json(['errors'=>$validator->errors()->all()]);
            }

            if ($validator->passes()) {
                $image = $request->file('image');
                if (isset($image))
                {
                    $currentDate = Carbon::now()->toDateString();
                    $imageName = 'client-'.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
                    if (!Storage::disk('public')->exists('image'))
                    {
                        Storage::disk('public')->makeDirectory('image');
                    }
                    $imagePath = Image::make($image)->resize(150,150)->stream();
                    Storage::disk('public')->put('image/'.$imageName,$imagePath);
                } else {
                    $imageName = "default_client_img.png";
                }

                // $data = Client::insert([
                //     "client_id" => $request->client_id,
                //     "nid" => $request->nid,
                //     "name" => $request->name,
                //     "image" => $imageName,
                //     "areas_id" => $request->areas_id,
                //     "area_code" => $request->area_code,
                //     "billing_address" => $request->billing_address,
                //     "contact_number" => $request->contact_number,
                //     "connection_date" => $request->connection_date,
                //     "connection_address" => $request->connection_address,
                //     "connection_fee" => $request->connection_fee,
                //     "packages_id" => $request->packages_id,
                //     "monthly_bill" => $request->monthly_bill,
                //     "created_at" => Carbon::now()
                // ]);

                $client = new Client();
                $client->client_id = $request->client_id;
                $client->nid = $request->nid;
                $client->name = $request->name;
                $client->image = $imageName;
                $client->areas_id = $request->areas_id;
                $client->area_code = $request->area_code;
                $client->billing_address = $request->billing_address;
                $client->contact_number = $request->contact_number;
                $client->connection_date = $request->connection_date;
                $client->connection_address = $request->connection_address;
                $client->connection_fee = $request->connection_fee;
                $client->packages_id = $request->packages_id;
                $client->monthly_bill = $request->monthly_bill;
                $client->save();

                $clients_id = $client->id;
                $BillID = rand(10000,99999);
                $month = date('m');
                $year = date('Y');

                $BillPayment = BillPayment::insert([
                        "clients_id" => $clients_id,
                        "month" => $month,
                        "year" => $year,
                        "billId" => $BillID,
                        "total_amt" =>  $request->monthly_bill,
                        "current_amt" =>  $request->monthly_bill,
                        "paid_amt" =>  0.00,
                        "due_amt" =>  $request->monthly_bill,
                        "payment_deadline" => $request->connection_date,
                        "created_at" => Carbon::now(),
                ]);

                if($client)
                {
                    return response()->json("success");
                }
            }
        }

        public function show()
        {
            $data = DB::table('clients')
                ->join('packages', 'clients.packages_id', '=', 'packages.id')
                ->join('areas', 'clients.areas_id', '=', 'areas.id')
                ->select('clients.*', 'packages.name as package', 'areas.name as area')
                ->orderBy('id', 'DESC')->orderBy('id', 'desc')->get();
            return response()->json([
                "draw" => 1,
                "recordsTotal" => 57,
                "recordsFiltered" => 57,
                "data" => $data,
            ]);
        }
        public function view(Request $request)
        {
            $id = $request->id;
            //$bills = DB::table('bill_payments')->where('clients_id', $id)->orderBy('id', "desc")->take(1)->get();
            $bills = DB::table('bill_payments')->where('clients_id', $id)->orderBy('clients_id', "desc")->get();
            $lastBill = DB::table('bill_payments')->where('clients_id', $id)->orderBy('clients_id', "desc")->take(1)->get();
            $clients = DB::table('clients')
                ->join('packages', 'clients.packages_id', '=', 'packages.id')
                ->join('areas', 'clients.areas_id', '=', 'areas.id')
                ->select('clients.*', 'packages.name as package', 'areas.name as area')
                ->where('clients.id', $id)->get();
            return response()->json([
                "lastBill" => $lastBill,
                "clients" => $clients,
                "bills" => $bills,
            ]);
        }
                

        public function edit(Request $request)
        {
            $id = $request->id;
            $data = DB::table('clients')
                ->join('packages', 'clients.packages_id', '=', 'packages.id')
                ->join('areas', 'clients.areas_id', '=', 'areas.id')
                ->select('clients.*', 'packages.name as package', 'areas.name as area')
                ->where('clients.id', $id)->get();
            return response()->json($data);
        }

        public function update(Request $request)
        {
            $validator = Validator::make($request->all(), [
                'client_id' => 'required',
                //'nid' => 'required',
                'name' => 'required',
                //'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10000',
                'areas_id' => 'required|numeric',
                'area_code' => 'required',
                'billing_address' => 'required',
                'contact_number' => 'required|numeric|min:10',
                'connection_address' => 'required',
                'connection_date' => 'required',
                'connection_fee' => 'required',
                'packages_id' => 'required|numeric',
                'monthly_bill' => 'required',
            ]);
            
            if ($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }
            if ($validator->passes()) {
                $image = $request->file('image');
                $client = Client::find($request->id);
                if (isset($image))
                {
                    $currentDate = Carbon::now()->toDateString();
                    $imageName = 'image-'.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
                    if (Storage::disk('public')->exists('image/'.$client->image))
                    {
                        Storage::disk('public')->delete('image/'.$client->image);
                    }
                    $imagePath = Image::make($image)->resize(150,150)->stream();
                    Storage::disk('public')->put('image/'.$imageName,$imagePath);
                } else {
                    $imageName = $client->image;
                }
                $data = Client::where('id', $request->id)->update([
                    "client_id" => $request->client_id,
                    "nid" => $request->nid,
                    "name" => $request->name,
                    "image" => $imageName,
                    "areas_id" => $request->areas_id,
                    "area_code" => $request->area_code,
                    "billing_address" => $request->billing_address,
                    "contact_number" => $request->contact_number,
                    "connection_address" => $request->connection_address,
                    "connection_date" => $request->connection_date,
                    "connection_fee" => $request->connection_fee,
                    "packages_id" => $request->packages_id,
                    "monthly_bill" => $request->monthly_bill,
                    "status" => $request->status,
                    "updated_at" => Carbon::now(),
                ]);
                if($data)
                {
                    return response()->json("success");
                }else{
                    return response()->json("query_fail");
                }
            }
        }

        public function payment_receive(Request $request)
        {
            $id = $request->id;
            $data = DB::table('bill_payments')
                ->join('clients', 'clients.id', '=', 'bill_payments.clients_id')
                ->select('clients.*', 'bill_payments.*', 'clients.id as clientid')
                ->where('bill_payments.clients_id', $id)->orderBy('clients_id', "desc")->take(1)->get();
            return response()->json($data);
        }

        public function LoadAllBill()
        {
            $data = DB::table('bill_payments')->get();
            return response()->json($data);
        }

        public function delete_view(Request $request)
        {
            $id = $request->id;
            $data = Client::find($id);
            return response()->json($data);
        }

        public function delete(Request $request)
        {
            $id = $request->id;
            $client = Client::find($id);
            if (Storage::disk('public')->exists('image/'.$client->image))
            {
                Storage::disk('public')->delete('image/'.$client->image);
            }
            $client->delete();
            if($client)
            {
                return response()->json("success");
            }
        }
}

