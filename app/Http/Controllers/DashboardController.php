<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\BillPayment;
use App\ClientBill;
use Carbon\Carbon;
use App\Package;
use App\Client;
use App\Area;
use Validator;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('dashboard.home');
    }

    public function total_clients()
    {
    	$clients = Client::all();
        $activeClients = DB::table('clients')->where('status', 1)->get();
        $inactiveClients = DB::table('clients')->where('status', 0)->get();

    	return response()->json([
    		'clients' => count($clients),
    		'activeClients' => count($activeClients),
    		'inactiveClients' => count($inactiveClients),
    	]);
    }

    public function total_bill()
    {
        $clients = DB::table('clients')->get();

        $totalPaidAmount = 0;
        $totalDueAmount = 0;
    	$i = 0;
    	for ($i=0; $i < count($clients); $i++) 
    	{
    		$id = $clients[$i]->id;
    		$bills = DB::table('bill_payments')->where('clients_id', $id)->orderBy('id', "desc")->take(1)->get();
    		$totalPaidAmount += $bills[0]->paid_amt;
    		$totalDueAmount += $bills[0]->due_amt;
    	}
    	$totalBill = $totalPaidAmount + $totalDueAmount;

    	return response()->json([
    		'totalBill' => $totalBill,
    		'totalPaidAmount' => $totalPaidAmount,
    		'totalDueAmount' => $totalDueAmount,
    	]);
    }

    public function monthly_bill()
    {
    	$month = date('m');
        $bills = DB::table('bill_payments')->where('month', $month)->get();
        
        $monthlyPaid = 0;
        $monthlyDue = 0;
    	$i = 0;
    	for ($i=0; $i < count($bills); $i++) 
    	{
    		$id = $bills[$i]->id;
    		$monthlyPaid += $bills[$i]->paid_amt;
    		$monthlyDue += $bills[$i]->due_amt;
    	}
    	$monthlyBill = $monthlyPaid + $monthlyDue;

    	return response()->json([
    		"monthlyBill" => $monthlyBill,
    		"monthlyPaid" => $monthlyPaid,
    		"monthlyDue" => $monthlyDue,
    	]);
    }

}
