@extends('dashboard.master')
@section('title')
	Package
@endsection
@section('mainContent')
	<div class="main-container">
		<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
			<div class="row clearfix progress-box">
				<div class="col-lg-4 col-md-4 col-sm-12 mb-30">
					<div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
						<div class="clearfix mb-20 bg-info box-shadow pd-20">
							<div class="pull-center">
								<h5 class="text-white"> Add New Package </h5>
							</div>
						</div>
						<hr>
						<form method="POST" action="{{ route('package.store') }}" id="package_add_data">
                    	@csrf
                    	@method('POST')
							<div class="form-group row">
								<label class="col-sm-12 col-md-2 col-form-label"> Name </label>
								<div class="col-sm-12 col-md-10">
									<input class="form-control" name="name" type="text" placeholder="Enter package name">
								</div>
								<ul id="package_add_errors">
									<!-- errors message here -->
								</ul>
							</div>
							<hr>
							<div class="form-group row">
								<div class="col-sm-12 col-md-10">
									<input class="btn btn-info" type="submit" value="Add Package">
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-12 mb-30">
					<div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
						<div class="clearfix mb-20 bg-info box-shadow pd-20">
							<div class="pull-center">
								<h5 class="text-white"> All Packages List </h5>
							</div>
						</div>
						<table width="100%" class="table-bordered stripe hover text-center">
							<thead>
								<tr>
									<th> #Sl. </th>
									<th> Name </th>
									<th> Status </th>
									<th> Actions </th>
								</tr>
							</thead>
							<tbody id="show_all_packages">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- Edit Modal -->
	<div class="modal fade" id="package_edit_modal" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog" role="document">
		    <div class="modal-content">
			    <div class="modal-header">
			        <h5 class="modal-title">Edit Package</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			    </div>
			    <form method="PUT" action="{{ route('package.update') }}" id="package_update_data">
	        	@csrf
	        	@method('PUT')
				    <div class="modal-body">
				    	<ul id="update_add_errors">
							<!-- errors message here -->
						</ul>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label"> Name </label>
							<div class="col-sm-12 col-md-10">
								<input type="hidden" class="form-control" name="id" id="package_edit_id">
								<input class="form-control" name="name" id="package_edit_name" type="text">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label"> Change Status </label>
							<div class="col-sm-12 col-md-10">
								<select name="status" id="package_edit_status" class="form-control">
									<option value="1"> Active </option>
									<option value="0"> Inactive </option>
								</select>
							</div>
						</div>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal"> Close </button>
				        <button type="submit" class="btn btn-info"> Update </button>
				    </div>
			    </form>
		    </div>
	  	</div>
	</div>

	<!-- Edit Modal -->
	<div class="modal fade" id="package_delete_modal" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog" role="document">
		    <div class="modal-content">
			    <form method="DELETE" action="{{ route('package.destroy') }}" id="package_delete_data">
	        	@csrf
	        	@method('DELETE')
				    <div class="modal-body">
						<input type="hidden" class="form-control" name="id" id="package_delete_id">
						<h5 style="color:red;"> Are you sure want delete this??? </h5>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-success" data-dismiss="modal"> No </button>
				        <button type="submit" class="btn btn-danger"> Yes! Delete </button>
				    </div>
			    </form>
		    </div>
	  	</div>
	</div>
@endsection