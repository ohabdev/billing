@extends('dashboard.master')
@section('title')
	Employees
@endsection
@section('mainContent')
	<div class="main-container">
		<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
			<div class="min-height-200px">
				<!-- Export Datatable start -->
				<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
					<div class="clearfix mb-20 bg-info box-shadow pd-20">
						<div class="pull-center">
							<h4 class="text-white text-center"> Employees List </h4>
						</div>
					</div>
					<button class="btn btn-sm btn-success"  data-toggle="modal" data-target="#employee_add_modal"> <i class="fa fa-plus"></i> Add New Employee </button><hr>
					<div class="row">
						<table class="table table-bordered stripe hover multiple-select-row data-table-export nowrap">
							<thead>
								<tr>
									<th> #SL </th>
									<th> Employee Name </th>
									<th> Photo </th>
									<th> Mobile Number/Login ID </th>
									<th> Address </th>
									<th> Hire Date </th>
									{{-- <th class="table-plus datatable-nosort">Actions</th> --}}
								</tr>
							</thead>
							<tbody id="show_all_employee">
								
							</tbody>
							<tbody>
								<tr>
									<th> #SL </th>
									<th> Employee Name </th>
									<th> Photo </th>
									<th> Mobile Number/Login ID </th>
									<th> Address </th>
									<th> Hire Date </th>
									{{-- <th class="table-plus datatable-nosort">Actions</th> --}}
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<!-- Export Datatable End -->
			</div>
		</div>
	</div>
	<!-- Add Modal -->
	<div class="modal fade" id="employee_add_modal" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog modal-lg">
		    <div class="modal-content">
				<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
					<div class="row clearfix progress-box">
						<div class="col-lg-12 col-md-12 col-sm-12 mb-30">
							<div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
								<div class="clearfix mb-20 bg-info box-shadow pd-20">
									<div class="pull-center">
										<h4 class="text-white text-center"> Add New Employee </h4>
									</div>
								</div>
								<ul class="employee_add_errors">
									
								</ul>
								<form method="POST" action="{{ route('employee.store') }}" enctype="multipart/form-data" id="employee_add_data">
		                    	@csrf
		                    	@method('POST')
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">  Full Name : </label>
												<input type="text" name="name" class="form-control" placeholder="Enter Full Name">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label"> Hire Date : </label>
												<input type="date" name="hire_date" class="form-control">
											</div>
										</div>							
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">  Phone Number/Login ID : </label>
												<input type="text" name="phone_number" class="form-control" placeholder="Enter phone number">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label"> Password : </label>
												<input type="password" name="password" class="form-control" placeholder="Enter Password">
											</div>
										</div>							
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">  Select Image : </label>
												<input type="file" name="image" class="form-control">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">  Address : </label>
												<input type="text" name="address" class="form-control" placeholder="Enter address">
											</div>
										</div>							
									</div>
									<hr>
									<div class="form-group row">
										<div class="col-sm-12 col-md-4">
											<button class="btn btn-lg btn-danger" type="button" data-dismiss="modal"> Cancel </button>
											<button class="btn btn-lg btn-info" type="submit"> Save </button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
		    </div>
	  	</div>
	</div>
	<!-- View Modal -->
	<div class="modal fade" id="employee_view_modal" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog modal-lg">
		    <div class="modal-content">
				<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
					<div class="row clearfix progress-box">
						<div class="col-lg-12 col-md-12 col-sm-12 mb-30">
							<div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
								<div class="clearfix mb-20 bg-info box-shadow pd-20">
									<div class="pull-center">
										<h4 class="text-white text-center"> Employee Info </h4>
									</div>
								</div>
	                    		<div class="text-center">
	                    			<img width="100" height="80" class="view_employee_image" style="border-radius:50%; border:1px solid green;">
	                    		</div>
	                    		<hr>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5> Full Name : </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_employee_name"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5> Hire Date : </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_employee_hire_date"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5> Phone Number : </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_employee_phone_number"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5>  Address : </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_employee_address"></h6>
										</div>
									</div>						
								</div>
								<hr>
								<div class="form-group row">
									<div class="col-sm-12 col-md-4">
										<button class="btn btn-lg btn-danger" type="button" data-dismiss="modal"> Close </button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		    </div>
	  	</div>
	</div>
	<!-- Edit Modal -->
	<div class="modal fade" id="employee_edit_modal" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog modal-xl">
		    <div class="modal-content">
				<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
					<div class="row clearfix progress-box">
						<div class="col-lg-12 col-md-12 col-sm-12 mb-30">
							<div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
								<div class="clearfix mb-20 bg-info box-shadow pd-20">
									<div class="pull-center">
										<h4 class="text-white text-center"> Edit Employee Info </h4>
									</div>
								</div>
								<ul class="employee_update_errors">
									
								</ul>
								<form name="client_form" method="POST" action="{{ route('employee.update') }}" enctype="multipart/form-data" id="employee_update_data">
		                    	@csrf
		                    		<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">  Full Name : </label>
												<input type="hidden" name="id" class="form-control edit_employee_id">
												<input type="text" name="name" class="form-control edit_employee_name" placeholder="Enter Full Name">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label"> Hire Date : </label>
												<input type="date" name="hire_date" class="form-control edit_employee_hire_date">
											</div>
										</div>							
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">  Phone Number/Login ID : </label>
												<input type="text" name="phone_number" class="form-control edit_employee_phone_number" placeholder="Enter phone_number">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label"> Password : </label>
												<input type="password" name="password" class="form-control edit_employee_password" placeholder="Enter Password">
											</div>
										</div>							
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label"> Change Image : </label>
												<img width="70" height="50" id="edit_employee_image" style="border-radius: 50%;">
												<input type="file" name="image"  class="form-control">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">  Address : </label>
												<input type="text" name="address" class="form-control edit_employee_address" placeholder="Enter address">
											</div>
										</div>							
									</div>
									<hr>
									<div class="form-group row">
										<div class="col-sm-12 col-md-4">
											<button class="btn btn-lg btn-danger" type="button" data-dismiss="modal"> Cancel </button>
											<button class="btn btn-lg btn-info" type="submit"> Update </button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
		    </div>
	  	</div>
	</div>
	<!-- Delete Modal -->
	<div class="modal fade" id="employee_delete_modal" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog" role="document">
		    <div class="modal-content">
			    <form method="DELETE" action="{{ route('employee.destroy') }}" id="employee_delete_data">
	        	@csrf
	        	@method('DELETE')
				    <div class="modal-body">
						<input type="hidden" class="form-control" name="id" id="employee_delete_id">
						<h5 style="color:red;"> Are you sure want delete this??? </h5>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-success" data-dismiss="modal"> No </button>
				        <button type="submit" class="btn btn-danger"> Yes! Delete </button>
				    </div>
			    </form>
		    </div>
	  	</div>
	</div>
@endsection
