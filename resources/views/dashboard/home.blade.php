@extends('dashboard.master')
@section('title')
	Dashboard
@endsection
@section('mainContent')
	<div class="main-container">
		<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
			<div class="row clearfix">
				<div class="col-lg-12 col-md-6 col-sm-12 mb-30">
					<div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
						<div class="project-info clearfix">
							<div class="project-info-left">
								<div class="icon box-shadow bg-blue text-white">
									<i class="fa fa-briefcase"></i>
								</div>
							</div>
							<div class="project-info-right">
								<span class="no text-blue weight-500 font-24 monthly_bill"></span>
								<p class="weight-400 font-18">This Month Total Bill</p>
							</div>
						</div>
						<hr>	
						<div class="row clearfix progress-box">
							<div class="col-lg-6 col-md-6 col-sm-12 mb-30">
								<div class="bg-white pd-20 border-radius-5 height-100-p">
									<div class="project-info clearfix">
										<div class="project-info-left">
											<div class="icon box-shadow bg-light-green text-white">
												<i class="fa fa-money"></i>
											</div>
										</div>
										<div class="project-info-right">
											<span class="no text-green weight-500 font-24 monthly_paid"></span>
											<p class="weight-400 font-18" style="color:green;"> Paid Amount </p>
										</div>
									</div>
									<div class="project-info-progress">
										<div class="row clearfix">
											<div class="col-sm-6 text-right weight-500 font-14 text-muted monthly_paid"></div>
										</div>
										<div class="progress" style="height: 10px;">
											<div class="progress-bar bg-light-green progress-bar-striped progress-bar-animated monthly_paid_percent" role="progressbar" style="width: 20%;" aria-valuenow="500" aria-valuemin="0" aria-valuemax="28900"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 mb-30">
								<div class="bg-white pd-20 border-radius-5 height-100-p">
									<div class="project-info clearfix">
										<div class="project-info-left">
											<div class="icon box-shadow bg-light-orange text-white">
												<i class="fa fa-money"></i>
											</div>
										</div>
										<div class="project-info-right">
											<span class="no text-light-orange weight-500 font-24 monthly_due"></span>
											<p class="weight-400 font-18" style="color:red;"> Due Amount </p>
										</div>
									</div>
									<div class="project-info-progress">
										<div class="row clearfix">
											<div class="col-sm-6 text-right weight-500 font-14 text-muted monthly_due"></div>										
										</div>
										<div class="progress" style="height: 10px;">
											<div class="progress-bar bg-light-orange progress-bar-striped progress-bar-animated monthly_due_percent" role="progressbar" style="width: 0%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row clearfix progress-box">
				<div class="col-lg-6 col-md-6 col-sm-12 mb-30">
					<div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
						<div class="project-info clearfix">
							<div class="project-info-left">
								<div class="icon box-shadow bg-blue text-white">
									<i class="fa fa-handshake-o"></i>
								</div>
							</div>
							<div class="project-info-right">
								<span class="no text-light-green weight-500 font-24 total_clients"></span>
								<p class="weight-400 font-18"> Total Clients</p>
							</div>
						</div>
						<hr>
						<div class="row clearfix progress-box">
							<div class="col-lg-6 col-md-6 col-sm-12 mb-30">
								<div class="bg-white pd-20 border-radius-5 height-100-p">
									<div class="project-info clearfix">
										<div class="project-info-left">
											<div class="icon bg-light-green text-white">
												<i class="fa fa-users"></i>
											</div>
										</div>
										<div class="project-info-right">
											<span class="no text-green weight-500 font-24 activeClients"></span>
											<p class="weight-400 font-18" style="color:green;"> Active Clients </p>
										</div>
									</div>
									<div class="project-info-progress">
										<div class="row clearfix">
											<div class="col-sm-6 text-right weight-500 font-14 text-muted activeClients"></div>
										</div>
										<div class="progress" style="height: 10px;">
											<div class="progress-bar bg-light-green progress-bar-striped progress-bar-animated active_vs_total_clients" role="progressbar" style="width: 50%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 mb-30">
								<div class="bg-white pd-20 border-radius-5 height-100-p">
									<div class="project-info clearfix">
										<div class="project-info-left">
											<div class="icon bg-light-orange text-white">
												<i class="fa fa-users"></i>
											</div>
										</div>
										<div class="project-info-right">
											<span class="no text-light-orange weight-500 font-24 inactiveClients"></span>
											<p class="weight-400 font-18" style="color:red;"> Inactive Clients </p>
										</div>
									</div>
									<div class="project-info-progress">
										<div class="row clearfix">
											<div class="col-sm-6 text-right weight-500 font-14 text-muted inactiveClients"></div>
										</div>
										<div class="progress" style="height: 10px;">
											<div class="progress-bar bg-light-orange progress-bar-striped progress-bar-animated inactive_vs_total_clients" role="progressbar" style="width: 80%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 mb-30">
					<div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
						<div class="project-info clearfix">
							<div class="project-info-left">
								<div class="icon box-shadow bg-blue text-white">
									<i class="fa fa-money"></i>
								</div>
							</div>
							<div class="project-info-right">
								<span class="no text-light-green weight-500 font-24 total_bill"></span>
								<p class="weight-400 font-18"> Total Bill Amount</p>
							</div>
						</div>
						<hr>	
						<div class="row clearfix progress-box">
							<div class="col-lg-6 col-md-6 col-sm-12 mb-30">
								<div class="bg-white pd-20 border-radius-5 height-100-p">
									<div class="project-info clearfix">
										<div class="project-info-left">
											<div class="icon bg-light-green text-white">
												<i class="fa fa-money"></i>
											</div>
										</div>
										<div class="project-info-right">
											<span class="no text-green weight-500 font-24 total_paid_amt"></span>
											<p class="weight-400 font-18" style="color:green;"> Paid Amount </p>
										</div>
									</div>
									<div class="project-info-progress">
										<div class="row clearfix">
											<div class="col-sm-6 text-right weight-500 font-14 text-muted total_paid_amt"></div>
										</div>
										<div class="progress" style="height: 10px;">
											<div class="progress-bar bg-light-green progress-bar-striped progress-bar-animated paid_vs_total_month" role="progressbar" style="width: 50%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 mb-30">
								<div class="bg-white pd-20 border-radius-5 height-100-p">
									<div class="project-info clearfix">
										<div class="project-info-left">
											<div class="icon bg-light-orange text-white">
												<i class="fa fa-money"></i>
											</div>
										</div>
										<div class="project-info-right">
											<span class="no text-light-orange weight-500 font-24 total_due_amt"></span>
											<p class="weight-400 font-18" style="color:red;"> Due Amount </p>
										</div>
									</div>
									<div class="project-info-progress">
										<div class="row clearfix">
											<div class="col-sm-6 text-right weight-500 font-14 text-muted total_due_amt"></div>
										</div>
										<div class="progress" style="height: 10px;">
											<div class="progress-bar bg-light-orange progress-bar-striped progress-bar-animated due_vs_total_month" role="progressbar" style="width: 80%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection