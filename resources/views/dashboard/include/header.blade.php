<div class="pre-loader"></div>
<div class="header clearfix">
	<div class="header-right">
		<div class="brand-logo">
			<a href="{{ url('/') }}">
				<img src="assets/vendors/images/logo.png" alt="" class="mobile-logo" style="max-height: 50px">
			</a>
		</div>
		<div class="menu-icon">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
		<div class="user-info-dropdown">
			<div class="dropdown">
				<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
					<span class="user-icon"><i class="fa fa-user-o"></i></span>
					<span class="user-name">{{ Auth::user()->name }}</span>
				</a>
				<div class="dropdown-menu dropdown-menu-right">
					<a class="dropdown-item" href="profile.php"><i class="fa fa-user-md" aria-hidden="true"></i> Profile</a>
					<a class="dropdown-item" href="{{ route('logout') }}" 
						onclick="event.preventDefault(); 
                        document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out" aria-hidden="true"></i> Log Out
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
				</div>
			</div>
		</div>
	</div>
</div>