<div class="left-side-bar">
	<div class="brand-logo">
		<a href="{{ route('dashboard') }}">
			<img src="assets/vendors/images/deskapp-logo.png" alt="">
		</a>
	</div>
	<div class="menu-block customscroll">
		<div class="sidebar-menu">
			<ul id="accordion-menu">
				<li>
					<a href="{{ route('dashboard') }}" class="dropdown-toggle no-arrow">
						<i class="icon-copy fa fa-dashboard" aria-hidden="true"></i><span class="mtext">Dashboard</span>
					</a>
				</li>
				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<i class="icon-copy fa fa-group" aria-hidden="true"></i><span class="mtext">Clients</span>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('client.index') }}">Clients List</a></li>
						<li><a href="{{ route('client_report.index') }}">Client Reports</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<i class="icon-copy fa fa-credit-card-alt" aria-hidden="true"></i><span class="mtext">Billiing & Payment</span>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('bill_payment.index') }}">Generate Bill</a></li>
						<li><a href="advanced-components.php">Bill Archive </a></li>
						<li><a href="form-wizard.php">Pyament Receive</a></li>
						<li><a href="form-wizard.php">Reports</a></li>
						<li><a href="form-wizard.php"> Send Message </a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<i class="icon-copy fa fa-cc" aria-hidden="true"></i><span class="mtext">General Account</span>
					</a>
					<ul class="submenu">
						<li><a href="form-basic.php">Transaction</a></li>
						<li><a href="advanced-components.php">Transaction Archive</a></li>
						<li><a href="form-wizard.php">Reports</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<i class="icon-copy fa fa-address-card" aria-hidden="true"></i><span class="mtext">Staff Profile</span>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('employee.index') }}">Staffs List</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<i class="icon-copy fa fa-cogs" aria-hidden="true"></i><span class="mtext"> Settings</span>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('package.index') }}"> Packages List </a></li>
						<li><a href="{{ route('area.index') }}"> Area List </a></li>
						<li><a href="{{ route('expense.index') }}">Expense List</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="footer-wrap bg-white pd-20 mb-0 border-radius-5 box-shadow">
			C-App - By <br>
			<a href="//salmansajib.com/" target="_blank">
				Md Salman Sajib
			</a>
		</div>
	</div>
</div>