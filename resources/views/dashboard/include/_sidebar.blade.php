<div class="left-side-bar">
	<div class="brand-logo">
		<a href="{{ url('/') }}">
			<img src="assets/vendors/images/deskapp-logo.png" alt="">
		</a>
	</div>
	<div class="menu-block customscroll">
		<div class="sidebar-menu">
			<ul id="accordion-menu">
				<li>
					<a href="./" class="dropdown-toggle no-arrow">
						<i class="icon-copy fa fa-dashboard" aria-hidden="true"></i><span class="mtext">ড্যাশবোর্ড</span>
					</a>
				</li>
				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<i class="icon-copy fa fa-group" aria-hidden="true"></i><span class="mtext">গ্রাহক</span>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('client.create') }}">গ্রাহক সংযোজন</a></li>
						<li><a href="{{ route('client.index') }}">গ্রাহক তালিকা</a></li>
						<li><a href="#">গ্রাহক প্রতিবেদন</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<i class="icon-copy fa fa-credit-card-alt" aria-hidden="true"></i><span class="mtext">বিলিং & পেমেন্ট</span>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('bill_payment.index') }}">বিল তৈরী</a></li>
						<li><a href="advanced-components.php">বিল আর্কাইভ</a></li>
						<li><a href="form-wizard.php">পেমেন্ট রিসিভ</a></li>
						<li><a href="form-wizard.php">প্রতিবেদন</a></li>
						<li><a href="form-wizard.php"> এসএমএস প্রেরণ</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<i class="icon-copy fa fa-cc" aria-hidden="true"></i><span class="mtext">সাধারণ হিসাব</span>
					</a>
					<ul class="submenu">
						<li><a href="form-basic.php">লেনদেন</a></li>
						<li><a href="advanced-components.php">লেনদেন আর্কাইভ</a></li>
						<li><a href="form-wizard.php">প্রতিবেদন</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<i class="icon-copy fa fa-address-card" aria-hidden="true"></i><span class="mtext"> স্টাফ প্রোফাইল</span>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('employee.index') }}">স্টাফ তালিকা</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="javascript:;" class="dropdown-toggle">
						<i class="icon-copy fa fa-cogs" aria-hidden="true"></i><span class="mtext"> সেটিংস</span>
					</a>
					<ul class="submenu">
						<li><a href="{{ route('package.index') }}"> প্যাকেজ তালিকা </a></li>
						<li><a href="{{ route('area.index') }}"> এরিয়া তালিকা </a></li>
						<li><a href="{{ route('expense.index') }}">খাত তালিকা</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="footer-wrap bg-white pd-20 mb-0 border-radius-5 box-shadow">
			C-App - By <br>
			<a href="//salmansajib.com/" target="_blank">
				Md Salman Sajib
			</a>
		</div>
	</div>
</div>