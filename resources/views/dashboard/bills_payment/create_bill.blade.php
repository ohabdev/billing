@extends('dashboard.master')
@section('title')
	Clients
@endsection
@section('mainContent')
<hr>
<div class="main-container">
	<div class="card">
		<div class="card-body">
			<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
				<div class="panel panel-default">
					<form class="panel-wizard" action="{{ route('bill_payment.store') }}" method="POST" id="client_create_bill">
							@csrf
							@method('POST')
						<ul id="bill_create_errors">
							<!-- errors message -->
						</ul>
						<div class="panel-body">
								<div class="row">
									<div class="col-sm-2">
										<div class="form-group">
											<label class="control-label"> Month</label>
											<select name="month" class="form-control" id="bill_month" required="">
												<option value="" selected="" disabled="">---</option>
												<option value="1">January</option>
												<option value="2">February</option>
												<option value="3">March</option>
												<option value="4">April</option>
												<option value="5">May</option>
												<option value="6">June</option>
												<option value="7">July</option>
												<option value="8">August</option>
												<option value="9">September</option>
												<option value="10">October</option>
												<option value="11">Novebver</option>
												<option value="12">December</option>
											</select>
										</div><!-- form-group -->
									</div><!-- col-sm-6 -->
									<div class="col-sm-2">
										<div class="form-group">
											<label class="control-label">Year</label>
											<input type="number" name="year" id="bill_year" required="" class="form-control">
										</div> 
									</div> 
									<div class="col-sm-3">
										<div class="form-group">
											<div class="form-group">
												<label class="control-label">Payment Deadline </label>
												<input type="date" name="payment_deadline" required="" class="form-control" id="last_paymentdate">
											</div>
										</div> 
									</div> 
								</div> 
						</div><!-- panel-body -->
						<div class="panel-footer">
							<button type="submit" class="btn btn-info" id="bill_create"><i class="fa fa-check-circle"></i> Generate Bill </button>
						</div><!-- panel-footer -->
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-body">
			<table id="billTable" class="table table-hover table-bordered table-stripped" width="100%">
				<thead>
					<tr>
						<th> Client ID# </th>
						<th> Bill ID# </th>
						<th> Year </th>
						<th> Month </th>
						<th> Total Amount </th>
						<th> Paid Amount </th>
						<th> Due Amount </th>
						<th> Monthly Bill </th>
						<th> Payment Deadline </th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th> Client ID# </th>
						<th> Bill ID# </th>
						<th> Year </th>
						<th> Month </th>
						<th> Total Amount </th>
						<th> Paid Amount </th>
						<th> Due Amount </th>
						<th> Monthly Bill </th>
						<th> Payment Deadline </th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
	
@endsection

