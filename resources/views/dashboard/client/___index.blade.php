<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>BILLINGS - Clients List</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="shortcut icon" href="assets/src/images/logo.png">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<link rel="stylesheet" href="assets/vendors/styles/style.css">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-119386393-1');
	</script>
	<link rel="stylesheet" type="text/css" href="assets/src/plugins/datatables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="assets/src/plugins/datatables/media/css/dataTables.bootstrap4.css">
	<link rel="stylesheet" type="text/css" href="assets/src/plugins/datatables/media/css/responsive.dataTables.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
			<script
  src="https://code.jquery.com/jquery-3.4.0.min.js"
  integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg="
  crossorigin="anonymous"></script>
</head>
<body>
	<div class="pre-loader"></div>
	<div class="header clearfix">
		<div class="header-right">
			<div class="brand-logo">
				<a href="{{ url('/') }}">
					<img src="assets/vendors/images/logo.png" alt="" class="mobile-logo">
				</a>
			</div>
			<div class="menu-icon">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
						<span class="user-icon"><i class="fa fa-user-o"></i></span>
						<span class="user-name">{{ Auth::user()->name }}</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<a class="dropdown-item" href="profile.php"><i class="fa fa-user-md" aria-hidden="true"></i> Profile</a>
						<a class="dropdown-item" href="profile.php"><i class="fa fa-cog" aria-hidden="true"></i> Setting</a>
						<a class="dropdown-item" href="faq.php"><i class="fa fa-question" aria-hidden="true"></i> Help</a>
						<a class="dropdown-item" href="{{ route('logout') }}" 
							onclick="event.preventDefault(); 
                            document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out" aria-hidden="true"></i> Log Out
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('dashboard.include.sidebar')
	<div class="main-container">
		<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
			<div class="min-height-200px">
				<!-- Export Datatable start -->
				<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
					<div class="clearfix mb-20 bg-info box-shadow pd-20">
						<div class="pull-center">
							<h4 class="text-white text-center"> Clients List </h4>
						</div>
					</div>
					<button class="btn btn-sm btn-success"  data-toggle="modal" data-target="#client_add_modal"> <i class="fa fa-plus"></i> Add New Client </button><hr>
					<div class="row">
						<table class="table table-bordered stripe hover multiple-select-row data-table-export nowrap">
							<thead>
								<tr>
									<th> #SL </th>
									<th> Client ID </th>
									<th> Name </th>
									<th> Photo </th>
									<th> Area </th>
									<th> Billing Address </th>
									<th> Phone </th>
									<th> Package </th>
									<th> Monthly Bill </th>
									<th class="table-plus datatable-nosort">Action</th>
								</tr>
							</thead>
							<tbody id="show_all_client">

							</tbody>
							<tbody>
								<tr>
									<th> #SL </th>
									<th> Client ID </th>
									<th> Name </th>
									<th> Photo </th>
									<th> Area </th>
									<th> Billing Address </th>
									<th> Phone </th>
									<th> Package </th>
									<th> Monthly Bill </th>
									<th class="table-plus datatable-nosort">Action</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<!-- Export Datatable End -->
			</div>
		</div>
	</div>
	<!-- js -->
	<script src="{{ asset('public/custom/js/client.js') }}"></script>
	<script src="{{ asset('public/custom/js/package.js') }}"></script>
	<script src="{{ asset('public/custom/js/area.js') }}"></script>
	<script src="assets/vendors/scripts/script.js"></script>
	<script src="assets/src/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script src="assets/src/plugins/datatables/media/js/dataTables.bootstrap4.js"></script>
	<script src="assets/src/plugins/datatables/media/js/dataTables.responsive.js"></script>
	<script src="assets/src/plugins/datatables/media/js/responsive.bootstrap4.js"></script>
	<!-- buttons for Export datatable -->
	<script src="assets/src/plugins/datatables/media/js/button/dataTables.buttons.js"></script>
	<script src="assets/src/plugins/datatables/media/js/button/buttons.bootstrap4.js"></script>
	<script src="assets/src/plugins/datatables/media/js/button/buttons.print.js"></script>
	<script src="assets/src/plugins/datatables/media/js/button/buttons.html5.js"></script>
	<script src="assets/src/plugins/datatables/media/js/button/buttons.flash.js"></script>
	<script src="assets/src/plugins/datatables/media/js/button/pdfmake.min.js"></script>
	<script src="assets/src/plugins/datatables/media/js/button/vfs_fonts.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script>
		$('document').ready(function(){
			$('.data-table').DataTable({
				scrollCollapse: true,
				autoWidth: false,
				responsive: true,
				columnDefs: [{
					targets: "datatable-nosort",
					orderable: false,
				}],
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"language": {
					"info": "_START_-_END_ of _TOTAL_ entries",
					searchPlaceholder: "Search"
				},
			});
			$('.data-table-export').DataTable({
				scrollCollapse: true,
				autoWidth: false,
				responsive: true,
				columnDefs: [{
					targets: "datatable-nosort",
					orderable: false,
				}],
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"language": {
					"info": "_START_-_END_ of _TOTAL_ entries",
					searchPlaceholder: "Search"
				},
				dom: 'Bfrtip',
				buttons: [
				'copy', 'csv', 'pdf', 'print'
				]
			});
			var table = $('.select-row').DataTable();
			$('.select-row tbody').on('click', 'tr', function () {
				if ($(this).hasClass('selected')) {
					$(this).removeClass('selected');
				}
				else {
					table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
			});
			var multipletable = $('.multiple-select-row').DataTable();
			$('.multiple-select-row tbody').on('click', 'tr', function () {
				$(this).toggleClass('selected');
			});
		});
	</script>

	<!-- Add Modal -->
	<div class="modal fade" id="client_add_modal" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog modal-xl">
		    <div class="modal-content">
				<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
					<div class="row clearfix progress-box">
						<div class="col-lg-12 col-md-12 col-sm-12 mb-30">
							<div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
								<div class="clearfix mb-20 bg-info box-shadow pd-20">
									<div class="pull-center">
										<h4 class="text-white text-center"> Add New Client </h4>
									</div>
								</div>
								<form name="client_form" method="POST" action="{{ route('client.store') }}" enctype="multipart/form-data" id="client_add_data">
		                    	@csrf
		                    		<h5 class="text-center"> General Information </h5>
		                    		<hr>
									<div class="row">
										<div class="col-sm-2">
											<div class="form-group">
												<label class="control-label">  Client ID : </label>
												<input type="text" name="client_id" class="form-control">
											</div>
										</div>
										<div class="col-sm-2">
											<div class="form-group">
												<label class="control-label"> NID Number : </label>
												<input type="text" name="nid" class="form-control">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Full Name : </label>
												<input type="text" name="name" class="form-control">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Image : </label>
												<input type="file" name="image" class="form-control">
											</div>
										</div>							
									</div>
									<div class="row">
										<div class="col-sm-2">
											<div class="form-group">
												<label class="control-label"> Area : </label>
												<select class="form-control areaIdName" name="areas_id" >
													<option> Select Area </option>
												</select>
											</div>
										</div>
										<div class="col-sm-2">
											<div class="form-group">
												<label class="control-label"> Area Code : </label>
												<input type="text" name="area_code" class="form-control">
											</div>
										</div>
										<div class="col-sm-8">
											<div class="form-group">
												<label class="control-label"> Billing Address : </label>
												<input type="text" name="billing_address" class="form-control">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Contact Number : </label>
												<input type="number" name="contact_number" class="form-control">
											</div>
										</div>
										<div class="col-sm-8">
											<div class="form-group">
												<label class="control-label"> Connection Address : </label>
												<input type="text" name="connection_address" class="form-control">
											</div>
										</div>
									</div>
									<hr>
									<h5 class="text-center"> Billing Information </h5>
									<hr>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Conncetion Date :  </label>
												<input type="date" name="connection_date" class="form-control">
											</div>
										</div>
										<div class="col-sm-8">
											<div class="form-group">
												<label class="control-label"> Connection Fee : </label>
												<input type="number" step="any" name="connection_fee" class="form-control">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Packages : </label>
												<select class="form-control packageIdName" name="packages_id" >
													<option> Select Package </option>
												</select>
											</div>								
										</div>
										<div class="col-sm-8">
											<div class="form-group">
												<label class="control-label"> Monthly Bill : </label>
												<input type="number" step="any" name="monthly_bill" class="form-control">
											</div>
										</div>
									</div>
									<hr>
									<div class="form-group row">
										<div class="col-sm-12 col-md-4">
											<button class="btn btn-lg btn-danger" type="button" data-dismiss="modal"> Cancel </button>
											<button class="btn btn-lg btn-info" type="submit"> Submit </button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
		    </div>
	  	</div>
	</div>
	<!-- View Modal -->
	<div class="modal fade" id="client_view_modal" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog modal-lg">
		    <div class="modal-content">
				<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
					<div class="row clearfix progress-box">
						<div class="col-lg-12 col-md-12 col-sm-12 mb-30">
							<div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
								<div class="clearfix mb-20 bg-info box-shadow pd-20">
									<div class="pull-center">
										<h4 class="text-white text-center"> Client Info </h4>
									</div>
								</div>
	                    		<h5 class="text-center"> General Information </h5>
	                    		<hr>
	                    		<div class="text-center">
	                    			<img width="100" height="80" class="view_client_photo" style="border-radius:50%; border:1px solid green;">
	                    		</div>
	                    		<hr>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5> Client ID : </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_client_id"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5> Name : </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_client_name"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5> NID Number : </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_nid_number"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5>  Area : </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_area"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5>  Area Code : </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_area_code"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5>  Connection Date: </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_connection_date"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5>  Connection Fee: </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_connection_fee"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5>  Connection Address : </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_connection_address"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5>  Package : </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_package"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5>  Monthly Bill : </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_monthly_bill"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5>  Contact Number: </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_contact_number">*</h6>
										</div>
									</div>						
								</div>
								<hr>
								<div class="form-group row">
									<div class="col-sm-12 col-md-4">
										<button class="btn btn-lg btn-danger" type="button" data-dismiss="modal"> Close </button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		    </div>
	  	</div>
	</div>
	<!-- Edit Modal -->
	<div class="modal fade" id="client_edit_modal" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog modal-xl">
		    <div class="modal-content">
				<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
					<div class="row clearfix progress-box">
						<div class="col-lg-12 col-md-12 col-sm-12 mb-30">
							<div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
								<div class="clearfix mb-20 bg-info box-shadow pd-20">
									<div class="pull-center">
										<h4 class="text-white text-center"> Edit Client </h4>
									</div>
								</div>
								<form name="client_form" method="POST" action="{{ route('client.update') }}" enctype="multipart/form-data" id="client_update_data">
		                    	@csrf
		                    		<h5 class="text-center"> General Information </h5>
		                    		<hr>
									<div class="row">
										<div class="col-sm-2">
											<div class="form-group">
												<label class="control-label">  Client ID : </label>
												<input type="hidden" name="id" class="form-control clientID">
												<input type="text" name="client_id"  class="form-control client_id">
											</div>
										</div>
										<div class="col-sm-2">
											<div class="form-group">
												<label class="control-label"> NID Number : </label>
												<input type="text" name="nid" class="form-control nid_number">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Full Name : </label>
												<input type="text" name="name" class="form-control client_name">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Change Image : </label>
												<img width="70" height="50" id="client_photo" style="border-radius: 50%;">
												<input type="file" name="image"  class="form-control">
											</div>
										</div>							
									</div>
									<div class="row">
										<div class="col-sm-2">
											<div class="form-group">
												<label class="control-label"> Area : </label>
												<select class="form-control areaIdName" name="areas_id" id="areas_id" >
													<option> Select Area </option>
												</select>
											</div>
										</div>
										<div class="col-sm-2">
											<div class="form-group">
												<label class="control-label"> Area Code : </label>
												<input type="text" name="area_code" class="form-control area_code">
											</div>
										</div>
										<div class="col-sm-8">
											<div class="form-group">
												<label class="control-label"> Billing Address : </label>
												<input type="text" name="billing_address" class="form-control billing_address">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Contact Number : </label>
												<input type="number" name="contact_number" class="form-control contact_number">
											</div>
										</div>
										<div class="col-sm-8">
											<div class="form-group">
												<label class="control-label"> Connection Address : </label>
												<input type="text" name="connection_address" class="form-control connection_address">
											</div>
										</div>
									</div>
									<hr>
									<h5 class="text-center"> Billing Information </h5>
									<hr>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Conncetion Date :  </label>
												<input type="date" name="connection_date" class="form-control connection_date">
											</div>
										</div>
										<div class="col-sm-8">
											<div class="form-group">
												<label class="control-label"> Connection Fee : </label>
												<input type="number" step="any" name="connection_fee" class="form-control connection_fee">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Packages : </label>
												<select class="form-control packageIdName" name="packages_id" id="packages_id">
													<option> Select Package </option>
												</select>
											</div>								
										</div>
										<div class="col-sm-8">
											<div class="form-group">
												<label class="control-label"> Monthly Bill : </label>
												<input type="number" step="any" name="monthly_bill" class="form-control monthly_bill">
											</div>
										</div>
									</div>
									<hr>
									<div class="form-group row">
										<div class="col-sm-12 col-md-4">
											<button class="btn btn-lg btn-danger" type="button" data-dismiss="modal"> Cancel </button>
											<button class="btn btn-lg btn-info" type="submit"> Update </button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
		    </div>
	  	</div>
	</div>

	<!-- Delete Modal -->
	<div class="modal fade" id="client_delete_modal" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog" role="document">
		    <div class="modal-content">
			    <form method="DELETE" action="{{ route('client.destroy') }}" id="client_delete_data">
	        	@csrf
	        	@method('DELETE')
				    <div class="modal-body">
						<input type="hidden" class="form-control" name="id" id="client_delete_id">
						<h5 style="color:red;"> Are you sure want delete this??? </h5>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-success" data-dismiss="modal"> No </button>
				        <button type="submit" class="btn btn-danger"> Yes! Delete </button>
				    </div>
			    </form>
		    </div>
	  	</div>
	</div>

</body>
</html>