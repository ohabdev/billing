@extends('dashboard.master')
@section('title')
	Client Add
@endsection
@section('mainContent')
	<div class="main-container">
		<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
			<div class="row clearfix progress-box">
				<div class="col-lg-12 col-md-12 col-sm-12 mb-30">
					<div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
						<div class="clearfix mb-20 bg-info box-shadow pd-20">
							<div class="pull-center">
								<h4 class="text-white text-center"> Add New Client </h4>
							</div>
						</div>
						<form name="client_form" method="POST" action="{{ route('client.store') }}" enctype="multipart/form-data" id="client_add_data">
                    	@csrf
                    		<h5 class="text-center"> General Information </h5>
                    		<hr>
							<div class="row">
								<div class="col-sm-2">
									<div class="form-group">
										<label class="control-label">  Client ID : </label>
										<input type="text" name="client_id" class="form-control">
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<label class="control-label"> NID Number : </label>
										<input type="text" name="nid" class="form-control">
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label"> Full Name : </label>
										<input type="text" name="name" class="form-control">
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label"> Image : </label>
										<input type="file" name="image" class="form-control">
									</div>
								</div>							
							</div>
							<div class="row">
								<div class="col-sm-2">
									<div class="form-group">
										<label class="control-label"> Area : </label>
										<select class="form-control areaIdName" name="areas_id" >
											<option> Select Area </option>
										</select>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<label class="control-label"> Area Code : </label>
										<input type="text" name="area_code" class="form-control">
									</div>
								</div>
								<div class="col-sm-8">
									<div class="form-group">
										<label class="control-label"> Billing Address : </label>
										<input type="text" name="billing_address" class="form-control">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label"> Contact Number : </label>
										<input type="number" name="contact_number" class="form-control">
									</div>
								</div>
								<div class="col-sm-8">
									<div class="form-group">
										<label class="control-label"> Connection Address : </label>
										<input type="text" name="connection_address" class="form-control">
									</div>
								</div>
							</div>
							<hr>
							<h5 class="text-center"> Billing Information </h5>
							<hr>
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label"> Conncetion Date :  </label>
										<input type="date" name="connection_date" class="form-control">
									</div>
								</div>
								<div class="col-sm-8">
									<div class="form-group">
										<label class="control-label"> Connection Fee : </label>
										<input type="number" step="any" name="connection_fee" class="form-control">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label"> Packages : </label>
										<select class="form-control packageIdName" name="packages_id" >
											<option> Select Package </option>
										</select>
									</div>								
								</div>
								<div class="col-sm-8">
									<div class="form-group">
										<label class="control-label"> Monthly Bill : </label>
										<input type="number" step="any" name="monthly_bill" class="form-control">
									</div>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-12 col-md-4">
									<button class="btn btn-lg btn-info" type="submit"> Submit </button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection



{{-- <!-- Extra large modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#client_add_modal">Extra large modal</button>

<div class="modal fade" id tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      ...
    </div>
  </div>
</div> --}}