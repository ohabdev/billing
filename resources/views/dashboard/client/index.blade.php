@extends('dashboard.master')
@section('title')
	Clients
@endsection
@section('mainContent')
	<div class="main-container">
		<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
			<div class="min-height-200px">
				<!-- Export Datatable start -->
				<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
					<div class="clearfix mb-20 bg-info box-shadow pd-20">
						<div class="pull-center">
							<h4 class="text-white text-center"> Clients List </h4>
						</div>
					</div>
					<button class="btn btn-sm btn-success"  data-toggle="modal" data-target="#client_add_modal"> <i class="fa fa-plus"></i> Add New Client </button>
					<hr>
					<div class="row">
						<table id="clientTable" class="table table-hover table-bordered table-stripped">
							<thead>
								<tr>
									<th> #SL </th>
									<th> Client ID </th>
									<th> Name </th>
									<th> Photo </th>
									<th> Area </th>
									<th> Billing Address </th>
									<th> Phone </th>
									<th> Package </th>
									<th> Monthly Bill </th>
									<th> Status </th>
									<th class="table-plus datatable-nosort">Action</th>
									<th> Bill Info </th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th> #SL </th>
									<th> Client ID </th>
									<th> Name </th>
									<th> Photo </th>
									<th> Area </th>
									<th> Billing Address </th>
									<th> Phone </th>
									<th> Package </th>
									<th> Monthly Bill </th>
									<th> Status </th>
									<th class="table-plus datatable-nosort">Action</th>
									<th> Bill Info </th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
				<!-- Export Datatable End -->
			</div>
		</div>
	</div>

	<!-- Add Modal -->
	<div class="modal fade" id="client_add_modal" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog modal-xl">
		    <div class="modal-content">
				<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
					<div class="row clearfix progress-box">
						<div class="col-lg-12 col-md-12 col-sm-12 mb-30">
							<div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
								<div class="clearfix mb-20 bg-info box-shadow pd-20">
									<div class="pull-center">
										<h4 class="text-white text-center"> Add New Client </h4>
									</div>
								</div>
								<form name="client_form" method="POST" action="{{ route('client.store') }}" enctype="multipart/form-data" id="client_add_data">
		                    	@csrf
		                    		<h5 class="text-center"> General Information </h5>
		                    		<hr>
									<div class="row">
										<div class="col-sm-2">
											<div class="form-group">
												<label class="control-label">  Client ID : </label>
												<input type="text" name="client_id" class="form-control">
											</div>
										</div>
										<div class="col-sm-2">
											<div class="form-group">
												<label class="control-label"> NID Number : </label>
												<input type="text" name="nid" class="form-control">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Full Name : </label>
												<input type="text" name="name" class="form-control">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Image : </label>
												<input type="file" name="image" class="form-control">
											</div>
										</div>							
									</div>
									<div class="row">
										<div class="col-sm-2">
											<div class="form-group">
												<label class="control-label"> Area : </label>
												<select class="form-control areaIdName" name="areas_id" >
													<option> Select Area </option>
												</select>
											</div>
										</div>
										<div class="col-sm-2">
											<div class="form-group">
												<label class="control-label"> Area Code : </label>
												<input type="text" name="area_code" class="form-control">
											</div>
										</div>
										<div class="col-sm-8">
											<div class="form-group">
												<label class="control-label"> Billing Address : </label>
												<input type="text" name="billing_address" class="form-control">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Contact Number : </label>
												<input type="number" name="contact_number" class="form-control">
											</div>
										</div>
										<div class="col-sm-8">
											<div class="form-group">
												<label class="control-label"> Connection Address : </label>
												<input type="text" name="connection_address" class="form-control">
											</div>
										</div>
									</div>
									<hr>
									<h5 class="text-center"> Billing Information </h5>
									<hr>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Conncetion Date :  </label>
												<input type="date" name="connection_date" class="form-control">
											</div>
										</div>
										<div class="col-sm-8">
											<div class="form-group">
												<label class="control-label"> Connection Fee : </label>
												<input type="number" step="any" name="connection_fee" class="form-control">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Packages : </label>
												<select class="form-control packageIdName" name="packages_id" >
													<option> Select Package </option>
												</select>
											</div>								
										</div>
										<div class="col-sm-8">
											<div class="form-group">
												<label class="control-label"> Monthly Bill : </label>
												<input type="number" step="any" name="monthly_bill" class="form-control">
											</div>
										</div>
									</div>
									<hr>
									<div class="form-group row">
										<div class="col-sm-12 col-md-4">
											<button class="btn btn-lg btn-danger" type="button" data-dismiss="modal"> Cancel </button>
											<button class="btn btn-lg btn-info" type="submit"> Submit </button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
		    </div>
	  	</div>
	</div>
	<!-- View Modal -->
	<div class="modal fade" id="client_view_modal" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog modal-lg">
		    <div class="modal-content">
				<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
					<div class="row clearfix progress-box">
						<div class="col-lg-12 col-md-12 col-sm-12 mb-30">
							<div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
								<div class="clearfix mb-20 bg-info box-shadow pd-20">
									<div class="pull-center">
										<h4 class="text-white text-center"> Client Info </h4>
									</div>
								</div>
	                    		<h5 class="text-center"> General Information </h5>
	                    		<hr>
	                    		<div class="text-center">
	                    			<img width="100" height="80" class="view_client_photo" style="border-radius:50%; border:1px solid green;">
	                    		</div>
	                    		<hr>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5> Client ID : </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_client_id"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5> Name : </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_client_name"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5> NID Number : </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_nid_number"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5>  Area : </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_area"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5>  Area Code : </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_area_code"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5>  Connection Date: </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_connection_date"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5>  Connection Fee: </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_connection_fee"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5>  Connection Address : </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_connection_address"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5>  Package : </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_package"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5>  Monthly Bill : </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_monthly_bill"></h6>
										</div>
									</div>						
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group pull-right">
											<h5>  Contact Number: </h5>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<h6 class="view_contact_number">*</h6>
										</div>
									</div>						
								</div>
								<hr>
								<div class="form-group row">
									<div class="col-sm-12 col-md-4">
										<button class="btn btn-lg btn-danger" type="button" data-dismiss="modal"> Close </button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		    </div>
	  	</div>
	</div>
	<!-- Edit Modal -->
	<div class="modal fade" id="client_edit_modal" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog modal-xl">
		  <div class="modal-content">
				<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
					<div class="row clearfix progress-box">
						<div class="col-lg-12 col-md-12 col-sm-12 mb-30">
							<div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
								<div class="clearfix mb-20 bg-info box-shadow pd-20">
									<div class="pull-center">
										<h4 class="text-white text-center"> Edit Client </h4>
									</div>
								</div>
								<form name="client_form" method="POST" action="{{ route('client.update') }}" enctype="multipart/form-data" id="client_update_data">
									@csrf
									<h5 class="text-center"> General Information </h5>
									<hr>
									<div class="row">
										<div class="col-sm-2">
											<div class="form-group">
												<label class="control-label">  Client ID : </label>
												<input type="hidden" name="id" class="form-control clientID">
												<input type="text" name="client_id"  class="form-control client_id">
											</div>
										</div>
										<div class="col-sm-2">
											<div class="form-group">
												<label class="control-label"> NID Number : </label>
												<input type="text" name="nid" class="form-control nid_number">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Full Name : </label>
												<input type="text" name="name" class="form-control client_name">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Change Image : </label>
												<img width="70" height="50" id="client_photo" style="border-radius: 50%;">
												<input type="file" name="image"  class="form-control">
											</div>
										</div>							
									</div>
									<div class="row">
										<div class="col-sm-2">
											<div class="form-group">
												<label class="control-label"> Area : </label>
												<select class="form-control areaIdName" name="areas_id" id="areas_id" >
													<option> Select Area </option>
												</select>
											</div>
										</div>
										<div class="col-sm-2">
											<div class="form-group">
												<label class="control-label"> Area Code : </label>
												<input type="text" name="area_code" class="form-control area_code">
											</div>
										</div>
										<div class="col-sm-8">
											<div class="form-group">
												<label class="control-label"> Billing Address : </label>
												<input type="text" name="billing_address" class="form-control billing_address">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Contact Number : </label>
												<input type="number" name="contact_number" class="form-control contact_number">
											</div>
										</div>
										<div class="col-sm-8">
											<div class="form-group">
												<label class="control-label"> Connection Address : </label>
												<input type="text" name="connection_address" class="form-control connection_address">
											</div>
										</div>
									</div>
									<hr>
									<h5 class="text-center"> Billing Information </h5>
									<hr>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Conncetion Date :  </label>
												<input type="date" name="connection_date" class="form-control connection_date">
											</div>
										</div>
										<div class="col-sm-8">
											<div class="form-group">
												<label class="control-label"> Connection Fee : </label>
												<input type="number" step="any" name="connection_fee" class="form-control connection_fee">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Packages : </label>
												<select class="form-control packageIdName" name="packages_id" id="packages_id">
													<option> Select Package </option>
												</select>
											</div>								
										</div>
										<div class="col-sm-8">
											<div class="form-group">
												<label class="control-label"> Monthly Bill : </label>
												<input type="number" step="any" name="monthly_bill" class="form-control monthly_bill">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label"> Change Active Status : </label>
												<select class="form-control client_status" name="status">
													<option value="1"> Active </option>
													<option value="0"> Inactive </option>
												</select>
											</div>								
										</div>
									</div>
									<hr>
									<div class="form-group row">
										<div class="col-sm-12 col-md-4">
											<button class="btn btn-lg btn-danger" type="button" data-dismiss="modal"> Cancel </button>
											<button class="btn btn-lg btn-info" type="submit"> Update </button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
		    </div>
	  	</div>
	</div>
	<!-- Delete Modal -->
	<div class="modal fade" id="client_delete_modal" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog" role="document">
		    <div class="modal-content">
			    <form method="DELETE" action="{{ route('client.destroy') }}" id="client_delete_data">
	        	@csrf
	        	@method('DELETE')
				    <div class="modal-body">
						<input type="hidden" class="form-control" name="id" id="client_delete_id">
						<h5 style="color:red;"> Are you sure want delete this??? </h5>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-success" data-dismiss="modal"> No </button>
				        <button type="submit" class="btn btn-danger"> Yes! Delete </button>
				    </div>
			    </form>
		    </div>
	  	</div>
	</div>

	<!-- Bill info view Modal -->
	<div class="modal fade" id="client_bill_modal" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog modal-xl">
		    <div class="modal-content">
				<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
					<div class="row clearfix progress-box">
						<div class="col-lg-12 col-md-12 col-sm-12 mb-30">
							<div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
		                    	<div class="row">
									<div class="col-sm-2 col-md-2">
										<img src="" class="img-circle img-offline img-responsive img-profile" height="84" alt="">
									</div> 
									<div class="col-sm-4 col-md-4">
										<ul class="list-unstyled social-list">
											<li><i class="fa fa-chevron-right"></i> ID Number: <span class="client_nid"></span></li>
											<li><i class="fa fa-chevron-right"></i> Connetion Date: <span class="connection_date"></span></li>
											<li><i class="fa fa-chevron-right"></i> Connetion Fee: <span class="connection_fee"></span></li>
											<li><i class="fa fa-chevron-right"></i> Package: <span class="package"></span></li>
											<li><i class="fa fa-chevron-right"></i> Monthly Bill: <span class="monthly_bill"></span></li>
										</ul>
									</div> 
									<div class="col-sm-4 col-md-4">
										<ul class="list-unstyled social-list">
											<li><i class="fa fa-chevron-right"></i> Mobile  Number: <span class="contact_number"></span></li>
											<li><i class="fa fa-chevron-right"></i> Area : <span class="area"></span></li>
											<li><i class="fa fa-chevron-right"></i> Connection Address: <span class="connection_address"></span></li>
											<li><i class="fa fa-chevron-right"></i> Billing Address: <span class="billing_address"></span></li>
										</ul>
									</div>
									<div class="col-sm-2 col-md-2">
										<span class="client_status"></span>
									</div>
									<div class="col-sm-12 col-md-12"><br>
										<!-- Nav tabs -->
										<ul class="nav nav-tabs">
											<li class="nav-item"><a class="nav-link active" href="#activities" data-toggle="tab"><strong>Last Month Bill</strong></a></li>
											<li class="nav-item"><a class="nav-link" href="#bills" data-toggle="tab"><strong>All Month Bill Info</strong></a></li>
										</ul>
										<br>
										<!-- Tab panes -->
										<div class="tab-content nopadding noborder">
											<div class="tab-pane active" id="activities">
												<table class="table table-hover table-bordered">
													<thead>
														<tr>
															<th> Bill ID# </th>
															<th>Month</th>
															<th>Year</th>
															<th>Total Bill</th>
															<th>Current Bill</th>
															<th>Paid Amount</th>
															<th>Due</th>
															<th>Pyament Deadline</th>
															<th width="20"></th>
														</tr>
													</thead>
													<tbody id="lastBill">
													</tbody>
												</table>
												</div><!-- tab-pane -->
												<div class="tab-pane" id="bills">
													<table id="basicTable" class="table  table-hover">
														<thead>
															<tr>
																<th> Bill ID# </th>
																<th>Month</th>
																<th>Year</th>
																<th>Total Bill</th>
																<th>Current Bill</th>
																<th>Paid Amount</th>
																<th>Due</th>
																<th>Payment Deadline</th>
																<th width="20"></th>
															</tr>
														</thead>
														<tbody id="show_bill_info">

														</tbody>
													</table>
												</div> 
												<div class="tab-pane" id="followers">
													<textarea class="form-control" rows="5"></textarea>
												</div> 
											</div><!-- tab-content -->
										</div> 
									</div>
									<a class="btn btn-danger" style="color:white;" data-dismiss="modal"> Cancel </a><br>		
							</div>
						</div>
					</div>
				</div>
		    </div>
	  	</div>
	</div>

	<!-- Payment recevie Modal -->
	<div class="modal fade" id="payment_receive_modal" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
		    	<form action="{{ route('bill_payment.update') }}" method="POST" id="payment_update_data">
	  			@csrf
			    	<div class="modal-header bg-success">
				        <h5 class="modal-title" style="color:white;"> <i class="fa fa-money"></i> Receive Payment</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
			      	<div class="modal-body">
			        	<div class="panel-body">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label"> Client Name</label>
										<input type="hidden" class="form-control clientid" name="clientid">
										<input type="text" class="form-control client_name" disabled="">
									</div><!-- form-group -->
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label"> Bill ID </label>
										<input type="text" name="billId" class="form-control billId" readonly="">
									</div><!-- form-group -->
								</div><!-- col-sm-6 -->
								
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label"> Month </label>
										<input type="text" class="form-control bill_month" disabled="">
									</div><!-- form-group -->
								</div><!-- col-sm-6 -->
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label"> Year </label>
										<input type="text"  class="form-control bill_year" disabled="">
									</div><!-- form-group -->
								</div><!-- col-sm-6 -->
							</div> 
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label"> Bill Amount </label>
										<input type="number" step="any" name="total_amt" class="form-control t_amt" id="totalAmt" readonly="">
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label"> Paid Amount </label>
										<input type="number" step="any" name="paid_amt" id="bill_paid_amt" class="form-control">
									</div> 
								</div> 
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label"> due Amount </label>
										<input type="number" step="any" name="due_amt" class="form-control d_amt current_due_amt" id="dueAmt" readonly="">
									</div> 
								</div> 
							</div> 
						</div>
			     	</div>
			      	<div class="modal-footer">
				        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				        <button type="submit" class="btn btn-success"> Received </button>
			      	</div>
			    </form>
			</div>
	  	</div>
	</div>
@endsection

