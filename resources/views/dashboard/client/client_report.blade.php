@extends('dashboard.master')
@section('title')
	Client Report
@endsection
@section('mainContent')
<div class="main-container">
	<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
	<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-btns" style="display: none; opacity: 0;">
					<a href="" class="panel-minimize tooltips" data-toggle="tooltip" title="" data-original-title="Minimize Panel"><i class="fa fa-minus"></i></a>
					<a href="" class="panel-close tooltips" data-toggle="tooltip" title="" data-original-title="Close Panel"><i class="fa fa-times"></i></a>
				</div><!-- panel-btns -->
				<h4 class="panel-title">Client Reports</h4><hr>
			</div>
			<div class="panel-body">
				<form class="panel-wizard" id="tabWizard" action="" method="post"><input type="hidden" name="_token" value="">				
					<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label class="control-label">Reports Name</label>
							<select name="report_name" data-placeholder="Choose One" id="range" class="form-control">
								<option value="" selected="" disabled="">---</option>
								<option value="reportbystatus">According to client status</option>
								<option value="reportbyarea">According to client area</option>
								<option value="reportbybill">Client Yearly Reports</option>
							</select>
						</div> 
					</div> 
				</div> 
				<div class="row">
					<div class="col-sm-3" id="area">
						<div class="form-group">
							<label class="control-label">Area</label>
							<select name="area" data-placeholder="Choose One" class="form-control">
								<option value="">Select Area</option>
								<option value="69">Bogra</option>
								<option value="69">Bogra</option>
								<option value="69">Bogra</option>
								<option value="69">Bogra</option>
								<option value="69">Bogra</option>
							</select>
						</div> 
					</div>
					<div class="col-sm-3" id="status" style="">
						<div class="form-group">
							<label class="control-label">Active Status</label>
							<select name="status" data-placeholder="Choose One" class="form-control">
								<option value="" selected="" disabled="">Select Status</option>
								<option value="active">Active</option>
								<option value="pending">Inactive</option>
							</select>
						</div> 
					</div> 
				</div> 
				<div class="row" id="client_id">
					<div class="col-sm-3">
						<div class="form-group">
							<label class="control-label">Client ID</label>
							<input name="client_id" type="text" class="form-control">
						</div> 
					</div>
					<div class="col-sm-3" id="year">
						<div class="form-group">
							<label class="control-label">Year</label>
							<input type="text" name="year" id="year" class="form-control">
						</div> 
					</div> 
				</div> 
			</form>
		</div><!-- panel-body -->
			<div class="panel-footer">
				<button class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
			</div><!-- panel-footer -->
		</div>
	</div>
</div>
@endsection

