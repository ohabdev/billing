@extends('dashboard.master')
@section('title')
	Expense
@endsection
@section('mainContent')
	<div class="main-container">
		<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
			<div class="row clearfix progress-box">
				<div class="col-lg-4 col-md-4 col-sm-12 mb-30">
					<div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
						<div class="clearfix mb-20 bg-info box-shadow pd-20">
							<div class="pull-center">
								<h5 class="text-white"> Add New Expense </h5>
							</div>
						</div>
						<hr>
						<form method="POST" action="{{ route('expense.store') }}" id="expense_add_data">
                    	@csrf
                    	@method('POST')
							<div class="form-group row">
								<label class="col-sm-12 col-md-2 col-form-label"> Name </label>
								<div class="col-sm-12 col-md-10">
									<input class="form-control" name="name" type="text" placeholder="Enter expense name">
								</div>
								<ul id="expense_add_errors">
									<!-- errors message here -->
								</ul>
							</div>
							<hr>
							<div class="form-group row">
								<div class="col-sm-12 col-md-10">
									<input class="btn btn-info" type="submit" value="Add Expense">
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-12 mb-30">
					<div class="bg-white pd-20 box-shadow border-radius-5 height-100-p">
						<div class="clearfix mb-20 bg-info box-shadow pd-20">
							<div class="pull-center">
								<h5 class="text-white text-center"> All Expenses List </h5>
							</div>
						</div>
						<table width="100%" class="table-bordered stripe hover text-center">
							<thead>
								<tr>
									<th> #Sl. </th>
									<th> Name </th>
									<th> Status </th>
									<th> Actions </th>
								</tr>
							</thead>
							<tbody id="show_all_expense">

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- Edit Modal -->
	<div class="modal fade" id="expense_edit_modal" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog" role="document">
		    <div class="modal-content">
			    <div class="modal-header">
			        <h5 class="modal-title">Edit Expense</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			    </div>
			    <form method="PUT" action="{{ route('expense.update') }}" id="expense_update_data">
	        	@csrf
	        	@method('PUT')
				    <div class="modal-body">
				    	<ul id="expense_update_errors">
							<!-- errors message-->
						</ul>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label"> Name </label>
							<div class="col-sm-12 col-md-10">
								<input type="hidden" class="form-control" name="id" id="expense_edit_id">
								<input class="form-control" name="name" id="expense_edit_name" type="text">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-12 col-md-2 col-form-label"> Change Status </label>
							<div class="col-sm-12 col-md-10">
								<select name="status" id="expense_edit_status" class="form-control">
									<option value="1"> Active </option>
									<option value="0"> Inactive </option>
								</select>
							</div>
						</div>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal"> Close </button>
				        <button type="submit" class="btn btn-info"> Update </button>
				    </div>
			    </form>
		    </div>
	  	</div>
	</div>

	<!-- Edit Modal -->
	<div class="modal fade" id="expense_delete_modal" tabindex="-1" role="dialog" aria-hidden="true">
	  	<div class="modal-dialog" role="document">
		    <div class="modal-content">
			    <form method="DELETE" action="{{ route('expense.destroy') }}" id="expense_delete_data">
	        	@csrf
	        	@method('DELETE')
				    <div class="modal-body">
						<input type="hidden" class="form-control" name="id" id="expense_delete_id">
						<h5 style="color:red;"> Are you sure want delete this??? </h5>
				    </div>
				    <div class="modal-footer">
				        <button type="button" class="btn btn-success" data-dismiss="modal"> No </button>
				        <button type="submit" class="btn btn-danger"> Yes! Delete </button>
				    </div>
			    </form>
		    </div>
	  	</div>
	</div>
@endsection