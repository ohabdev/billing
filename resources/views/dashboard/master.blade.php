<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>BILLINGS - Clients List</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="shortcut icon" href="{{ asset('public/assets') }}/src/images/logo.png">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<link rel="stylesheet" href="assets/vendors/styles/style.css">
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-119386393-1');
	</script>
	{{-- <link rel="stylesheet" type="text/css" href="{{ asset('public/assets') }}/src/plugins/datatables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets') }}/src/plugins/datatables/media/css/dataTables.bootstrap4.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets') }}/src/plugins/datatables/media/css/responsive.dataTables.css"> --}}
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>

	@include('dashboard.include.header')
	@include('dashboard.include.sidebar')
	@yield('mainContent')
	<script src="{{ asset('public/assets') }}/vendors/scripts/script.js"></script>
	<!-- js -->
	<script src="{{ asset('public/custom/js/package.js') }}"></script>
	<script src="{{ asset('public/custom/js/area.js') }}"></script>
	<script src="{{ asset('public/custom/js/employee.js') }}"></script>
	<script src="{{ asset('public/custom/js/expense.js') }}"></script>
	<script src="{{ asset('public/assets') }}/src/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
	<script src="{{ asset('public/assets') }}/src/plugins/datatables/media/js/dataTables.bootstrap4.js"></script>
	<script src="{{ asset('public/assets') }}/src/plugins/datatables/media/js/dataTables.responsive.js"></script>
	<script src="{{ asset('public/assets') }}/src/plugins/datatables/media/js/responsive.bootstrap4.js"></script>
	<!-- buttons for Export datatable -->
	<script src="{{ asset('public/assets') }}/src/plugins/datatables/media/js/button/dataTables.buttons.js"></script>
	<script src="{{ asset('public/assets') }}/src/plugins/datatables/media/js/button/buttons.bootstrap4.js"></script>
	<script src="{{ asset('public/assets') }}/src/plugins/datatables/media/js/button/buttons.print.js"></script>
	<script src="{{ asset('public/assets') }}/src/plugins/datatables/media/js/button/buttons.html5.js"></script>
	<script src="{{ asset('public/assets') }}/src/plugins/datatables/media/js/button/buttons.flash.js"></script>
	<script src="{{ asset('public/assets') }}/src/plugins/datatables/media/js/button/pdfmake.min.js"></script>
	<script src="{{ asset('public/assets') }}/src/plugins/datatables/media/js/button/vfs_fonts.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="{{ asset('public/custom/js/client.js') }}"></script>
	<script src="{{ asset('public/custom/js/client_bill.js') }}"></script>
	<script src="{{ asset('public/custom/js/bill_payment.js') }}"></script>
	<script src="{{ asset('public/custom/js/dashboard.js') }}"></script>
	<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	{{-- <script>
		$('document').ready(function(){
			$('.data-table').DataTable({
				scrollCollapse: true,
				autoWidth: false,
				responsive: true,
				columnDefs: [{
					targets: "datatable-nosort",
					orderable: false,
				}],
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"language": {
					"info": "_START_-_END_ of _TOTAL_ entries",
					searchPlaceholder: "Search"
				},
			});
			// var x = $('.data-table-export').DataTable({
			// 	scrollCollapse: true,
			// 	autoWidth: false,
			// 	responsive: true,
			// 	columnDefs: [{
			// 		targets: "datatable-nosort",
			// 		orderable: false,
			// 	}],
			// 	"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			// 	"language": {
			// 		"info": "_START_-_END_ of _TOTAL_ entries",
			// 		searchPlaceholder: "Search"
			// 	},
			// 	dom: 'Bfrtip',
			// 	buttons: [
			// 	'copy', 'csv', 'pdf', 'print'
			// 	]
			// });
			var table = $('.select-row').DataTable();
			$('.select-row tbody').on('click', 'tr', function () {
				if ($(this).hasClass('selected')) {
					$(this).removeClass('selected');
				}
				else {
					table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
			});
			var multipletable = $('.multiple-select-row').DataTable();
			$('.multiple-select-row tbody').on('click', 'tr', function () {
				$(this).toggleClass('selected');
			});
		});
	</script>
 --}}
</body>
</html>
