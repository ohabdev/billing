<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
			<title>BILLINGS - Login</title>
			<meta name="csrf-token" content="{{ csrf_token() }}">
			<link rel="shortcut icon" href="assets/src/images/logo.png">
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
			<link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel="stylesheet">
			<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
			<link rel="stylesheet" href="assets/vendors/styles/style.css">

			<!-- Global site tag (gtag.js) - Google Analytics -->
			<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>
			<script>
			  window.dataLayer = window.dataLayer || [];
			  function gtag(){dataLayer.push(arguments);}
			  gtag('js', new Date());

			  gtag('config', 'UA-119386393-1');
			</script>
	</head>
	<body>
		<div class="login-wrap customscroll d-flex align-items-center flex-wrap justify-content-center pd-20">
			<div class="login-box bg-white box-shadow pd-30 border-radius-5">
				<img src="assets/vendors/images/login-img.png" alt="login" class="login-img">
				<h2 class="text-center mb-30">Login</h2>
				<form method="POST" action="{{ route('login') }}">
                    @csrf
					<div class="input-group custom input-group-lg">
						<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
						<div class="input-group-append custom">
							<span class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i></span>
						</div>
					</div>
					<div class="input-group custom input-group-lg">
						<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
						<div class="input-group-append custom">
							<span class="input-group-text"><i class="fa fa-lock" aria-hidden="true"></i></span>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="input-group">
								<input class="btn btn-outline-primary btn-lg btn-block" type="submit" value="Sign In">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="forgot-password padding-top-10">
								@if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- js -->
		<script src="assets/vendors/scripts/script.js"></script>
	</body>
</html>